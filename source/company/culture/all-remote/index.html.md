---
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Remote Manifesto

All remote work promotes:

1. Hiring and working from all over the world *instead of* from a central location. 
1. Flexible working hours *over* set working hours.
1. Writing down and recording knowledge *over* oral explanations.
1. Written down processes *over* on-the-job training. 
1. Public sharing of information *over* need-to-know access.
1. Opening every document to change by anyone *over* top down control of documents. 
1. Asynchronous communication *over* synchronous communication.
1. The results of work *over* the hours put in. 
1. Formal communication channels *over* informal communication channels. 

While there is sometimes value in the items on the right, we have come to value the items on the left more.

## How remote work is changing the workforce

1. Documentation of knowledge
1. Fewer Meetings, if you miss one, they are recorded
1. Everything is public, everyone can contribute
1. More flexibility in daily life
1. Reduce interruptions to [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html)
1. Cost savings on office space and compensation; Reduce inequality due to bringing better paying jobs to lower cost regions
1. Encourages focus on results, not hours worked
1. Reduce environmental impact due to less commuting

### How it changes the organization

1. Knowledge is written down instead of passed orally
1. More asynchronous communication
1. Shorter and fewer meetings
1. More transparency within and outside the organization
1. Everything is public by default
1. More official communication, less informal
1. More recorded materials means fewer interruptions and less on-the-job training

### Advantages for employees
1. More flexibility in your daily life (kids, parents, friends, groceries, sports, deliveries)
1. No commuting time or stress
1. No money and time wasted on commuting (subway/bus fees, gas, car maintenance, tolls, etc)
1. Reduce the interruption stress
1. Traveling to other places without taking vacation (family, fun, etc.)
1. Free to relocate to other places
1. Some folks find it easier to communicate with rude colleagues remotely
1. Onboarding may be less stressful
1. Home food: better (sometimes) and cheaper
1. Cheaper taxes in some countries (for example in Belarus you will pay only ~5%)
1. Pants not required
1. Access to global jobs market - everywhere!

### Advantages for organizations
1. Hire great people irrespective of where they live
1. More effective employees since they have fewer distractions
1. More loyal employees
1. Save on office costs
1. Save on compensation due to hiring in lower cost regions
1. Selects for self-starters
1. Makes it easy to grow a company quickly
1. Encourages a focus on results, fewer meetings, more output
1. Cheaper taxes in some countries

### Advantages for the world

1. Reduce environmental impact due to no commuting
1. Reduce environmental impact due to less office space
1. Reduce inequality due to bringing better paying jobs to lower cost regions

### Disadvantages
1. Scares investors
1. Scares some partners
1. Scares some customers
1. Scares some potential employees, mostly senior non-technical hires
1. Onboarding may be harder, first month feels lonely (for some people)
1. Some people find it more difficult to work from in the same place where they sleep and watch films - dedicated workplace helps to switch the context
1. Some people find that remote working degrades communication skills
1. The need to prepare food
1. You may be paid in a currency different to the local one, which carries risk & obstacles; e.g. in the UK it's harder to get a mortgage

## Why is this possible now
1. Fast internet everywhere - 100Mb/s+ cable, 5GHz Wifi, 4G cellular
1. Video call software - Google Hangouts, Zoom
1. Virtual office - sococo.com
1. Mobile - Everyone has a computer in their pocket
1. Messaging apps - Slack, Mattermost, Zulip
1. Issue trackers - Trello, GitHub issues, GitLab issues
1. Suggestions - GitHub Pull Requests, GitLab Merge Requests
1. Static websites - GitHub Pages, GitLab Pages
1. English proficiency - More people are learning English
1. Increasing traffic congestion in cities

At large tech companies people on the same campus now routinely do video calls instead of traveling 10 minutes each way to a different building.

More ways to enable remote-only work are continuously being developed. One example is the evolution of speech-to-text conversion software, which is more accurate and faster than typing, thus making written communication easier and more effective. Reading was already faster than listening, but now the process of committing speech to text + reading by recipient can be the faster way to communicate even in a 1:1 communication. For one-to-many communication, typing + reading is already faster.

## What it is not

1. There is no main office or headquarters with multiple people *"The only way to not have people in a satellite office is not to have a main office."*
1. It does not mean working independently; teams should work together and communicate often and collaborate frequently.
1. It is not simply offshoring work; rather, you hire around the world.
1. It is not a management paradigm, it is still a normal hierarchical organization, however there is a focus on output instead of input.
1. It is not a substitute for human interaction; people still need to collaborate, have conversations, and feel like valuable members of a team.

## How to build and manage a dsitrbuted team 

<iframe width="560" height="315" src="https://www.youtube.com/embed/tSp5se9BudA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## What Have We Learned about Remote Working?

How do we manage our entire [team](/company/team/) remotely? Sid Sijbrandij, CEO, shares the [secrets to managing 200 employees in 200 locations](https://www.youtube.com/watch?v=e56PbkJdmZ8).

Our policy of remote work comes from our [value](/handbook/values/) of boring solutions and was a natural evolution of team members choosing to work from home. Remote work allowed for the development of our publicly viewable [handbook](/handbook/). We like efficiency and do not like having to explain things twice.

In on-site companies they take processes, camaraderie, and culture for granted and have it develop organically. In an all-remote company you have to organize it, this is hard to do but as you scale it becomes more efficient while the the on-site organic approach fizzles out.

We have had success bonding with our coworkers in real life through our [Summits](/company/culture/contribute/) that are organized every 9 months and our [Visiting Grants](/handbook/incentives/#sts=Visiting grant).

We have a daily [team call](/handbook/communication/#team-call) to talk about what we did outside of work. There is also a daily [Group Conversation](/handbook/people-operations/group-conversations/) in which one department shares their progress and challenges.

Our [expense policy](/handbook/spending-company-money/) provides for equipment, internet service, office space if they want that, and a local holiday party.

We encourage people to have [virtual coffee breaks](https://work.qz.com/1147877/remote-work-why-we-put-virtual-coffee-breaks-in-our-company-handbook/) and do a [monthly AMA with the CEO](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2649/diffs).

We have an extensive [onboarding template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) for new hires and organize a [GitLab 101](/company/culture/gitlab-101/) to ask questions.

## Tips for Working Remotely

Arguably the biggest advantage of working remotely and asynchronously is the
flexibility it provides. This makes it easy to **combine** work with your
personal life although it might be difficult to find the right **balance**.
This can be mitigated by either explicitly planning your time off or plan when
you do work. When you don't work it is recommended to make yourself unavailable
by turning off Slack and closing down your email client. Coworkers should
allow this to work by abiding by the [communication guidelines](/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus).

If you worked at an office before, now you lack a default group to go out to
lunch with. To look at it from a different perspective, now you can select who
you lunch with and who you do not lunch with. Haven't spoken to a good friend in
a while? Now you can have lunch together.

### Practical tips

1. People don't have to say when they are working.
1. Working long hours or weekends is not encouraged nor celebrated.
1. Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process.
1. Encourage non-work related communication (talking about private life on a team call).
1. Encourage group video calls for [bonding](/2015/04/08/the-remote-manifesto/).
1. Encourage [video calls](/2015/04/08/the-remote-manifesto/) between people (10 as part of onboarding).
1. Periodic summits with the whole company to get to know each other in an informal setting.
1. Encourage [teamwork and saying thanks](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
1. Assign new hires a buddy so they have someone to reach out to.
1. Allow everyone in the company to view and edit every document.
1. Every document is always in draft, don't wait with sharing it until it is done.
1. Encourage people to write information down.

### Coffee Break Calls

Understanding that working remotely leads to mostly work-related conversations
with fellow GitLabbers, everyone is encouraged to dedicate **a few hours a week**
to having social calls with any teammate - get to know who you work with,
talk about everyday things and share a virtual cuppa' coffee. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. The Coffee Break calls are different from the
[Random Room](/handbook/communication/#random-room) video chat, they are meant to give you the option
to have 1x1 calls with specific teammates who you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

You can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee break. The "Donut" bot will automatically send a message to two people in the channel every other Monday. Please schedule a chat together, and Donut will follow up for feedback.

Of course, you can also directly reach out to your fellow GitLabbers to schedule a coffee break in the #donut_be_strangers Slack channel or via direct message.

### Tips on Ergonomic Working

The goal of [office ergonomics](http://ergo-plus.com/office-ergonomics-10-tips-to-help-you-avoid-fatigue/) is to design your office work station so that it fits you and allows for a comfortable working environment for maximum productivity and efficiency. Since we all work from home offices, GitLab wants to ensure that each team member has the [supplies](/handbook/spending-company-money/) and knowledge to create an ergonomic home office.

Below are some tips from the [Mayo Clinic](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/office-ergonomics/art-20046169) how on how arrange your work station. If however, you develop any pains which you think might be related to your working position, please visit a doctor.

- **Chair**: Choose a chair that supports your spinal curves. Adjust the height of your chair so that your feet rest flat on the floor or on a footrest and your thighs are parallel to the floor. Adjust armrests so your arms gently rest on them with your shoulders relaxed.
- **Keyboard and mouse:** Place your mouse within easy reach and on the same surface as your keyboard. While typing or using your mouse, keep your wrists straight, your upper arms close to your body, and your hands at or slightly below the level of your elbows. Use keyboard shortcuts to reduce extended mouse use. If possible, adjust the sensitivity of the mouse so you can use a light touch to operate it. Alternate the hand you use to operate the mouse by moving the mouse to the other side of your keyboard. Keep regularly used objects close to your body to minimize reaching. Stand up to reach anything that can't be comfortably reached while sitting.
- **Telephone**: If you frequently talk on the phone and type or write at the same time, place your phone on speaker or use a headset rather than cradling the phone between your head and neck.
- **Footrest**: If your chair is too high for you to rest your feet flat on the floor — or the height of your desk requires you to raise the height of your chair — use a footrest. If a footrest is not available, try using a small stool or a stack of sturdy books instead.
- **Desk**: Under the desk, make sure there's clearance for your knees, thighs and feet. If the desk is too low and can't be adjusted, place sturdy boards or blocks under the desk legs. If the desk is too high and can't be adjusted, raise your chair. Use a footrest to support your feet as needed. If your desk has a hard edge, pad the edge or use a wrist rest. Don't store items under your desk. GitLab recommends having an adjustable standing desk to avoid any issues.
- **Monitor**: Place the monitor directly in front of you, about an arm's length away. The top of the screen should be at or slightly below eye level. The monitor should be directly behind your keyboard. If you wear bifocals, lower the monitor an additional 1 to 2 inches for more comfortable viewing. Place your monitor so that the brightest light source is to the side.

### Health and Fitness

It is sometimes hard to stay active when you work from your home. GitLab wants to ensure each team member takes care of themselves and dedicates time to stay healthy. Here are some tips that can help to stay healthy and active. You can also join the Slack channel `fitlab` to discuss your tips, challenges, results, etc. with other team members.

1. Try to step away from your computer and stretch your body every hour.
1. To avoid "Digital Eye Strain" follow the [20-20-20 Rule](https://www.healthline.com/health/eye-health/20-20-20-rule#definition). Every 20 minutes look into the distance (at least 20 feet/6 meters) for 20 seconds.
1. There are apps that will remind you to take a break or help you with your computer posture:
 - [PostureMinder](http://www.postureminder.co.uk/)(Windows)
 - [Time Out](https://itunes.apple.com/us/app/time-out-break-reminders/id402592703?mt=12)(macOS)
 - [Awareness](http://iamfutureproof.com/tools/awareness/)(macOS)
 - [SafeEyes](http://slgobinath.github.io/SafeEyes/)(GNU/Linux)
1. Move every day
  - Even when it can be hard to force yourself to move when working the whole day from your home try to go for a walk or do a short excersise for at least 15 minutes / day.
  - There are multiple activities that can be done within a short amount of time like rope jumping, lifting kettlebells, push-ups or sit-ups. It might also help to split the activity into multiple shorter sessions. You can use an app that helps you with the workout, e.g., [7 minute workout](https://7minuteworkout.jnj.com/).
1. Try to create a repeatable schedule that is consistent and try to make a habit out of it. Make sure you enjoy the activity.
1. It can also help to create a goal or challenge for yourself, e.g., registering for a race can force you to excercise.
1. Eat less refined sugar and simple carbs, eat complex carbs instead. Try to eat more vegetables. Don't go to the kitchen to eat something every 15 minutes (it’s a trap!). Keep junk food out of your house.
1. Have a water bottle with you at your desk. You will be more inclined to drink if it's available at all times.
1. Sleep well and take a nap during the day if you need one.

## Remote work in the news

1  Inc. - [Case Closed: Work-From-Home Is the World's Smartest Management Strategy](https://www.inc.com/geoffrey-james/case-closed-work-from-home-is-worlds-smartest-management-strategy.html)
1. HackerNoon - [Remote work doesn't scale... or does it?](https://hackernoon.com/remote-work-doesnt-scale-or-does-it-4a72ce2bb1f3) 
1. GitLab Magazine - [How to keep healthy communication habits in remote teams](https://medium.com/gitlab-magazine/how-to-keep-healthy-communication-habits-in-remote-teams-a19eca371952)
1. remotehabits.com - [Remote work Interviews](https://remotehabits.com/)
1. Medium - [On-premise tribes in shiny caves and the evoluation of Remote as a Service](https://medium.com/understanding-as-a-service-uaas/on-premise-people-and-shiny-caves-remote-as-a-service-97cff86382b6)