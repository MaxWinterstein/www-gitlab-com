---
layout: markdown_page
title: Handbook Changelog
---

### 2018-12-22
- [!17625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17625) Add viable
- [!17601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17601) Update DB architecture diagram
- [!17025](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17025) Release post - GitLab 11.6
- [!16645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16645) Add fake phishing or text process.

### 2018-12-21
- [!17613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17613) Update MPM handbook
- [!17612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17612) adds overview of how to record a WIR podcast
- [!17607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17607) Update MPM Handbook
- [!17604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17604) Remove quick links from categories page
- [!17603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17603) Enforce some relative links
- [!17600](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17600) Add Cloud Native Engineer regex to Distribution team member list
- [!17595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17595) Code contributor program handbook update
- [!17591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17591) Changing sales call to bi-weekly.
- [!17587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17587) adding a caveat to comp transparency
- [!17585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17585) updates to handbook for GTM system
- [!17582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17582) adding pubsec to the territory map section
- [!17577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17577) Assigning issues
- [!17576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17576) Updated Access Management Process to include info and use of Baseline Entitlements
- [!17521](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17521) Fixed typo on Invoice Owner
- [!17174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17174) Updating documentation for `security@` and ZenDesk
- [!17161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17161) Update security handbook for external contribution process
- [!17059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17059) Add text for ~"security request"
- [!16139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16139) Update priority board link for Manage team

### 2018-12-20
- [!17580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17580) update outdated schedule
- [!17558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17558) Update index.html.md
- [!17557](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17557) Add frontend theme call theme for January
- [!17554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17554) verify shorter
- [!17524](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17524) Updated reddit workflow
- [!17518](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17518) Update source/handbook/marketing/blog/index.html.md
- [!17513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17513) Update index.html.md - fix typo (missing word 'to')
- [!17499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17499) Guidance on activities to ensure direction/vision/roadmap are updated
- [!17498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17498) Fix internal link
- [!17367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17367) IT Operations Charter for Discussion
- [!17365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17365) Clarify Retention Definitions

### 2018-12-19
- [!17522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17522) changing quote-help to deal-desk chatter group.
- [!17519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17519) Update community response channels
- [!17517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17517) Update Community Response channels
- [!17512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17512) Remove the due-22nd label
- [!17510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17510) Current rate but adjust once a year.
- [!17505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17505) update territory ownership table
- [!17504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17504) Adding New Hire
- [!17502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17502) Updating methods of changing the forecast category and renewal forecast...
- [!17501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17501) Adding Clari to tech stack list on Business Ops home page
- [!17500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17500) Adding Clari to Tech Stack
- [!17494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17494) Reddit handbook update
- [!17493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17493) Updates to Calc
- [!17468](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17468) Update index.html.md with details on events & template
- [!17462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17462) Updated typo
- [!17438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17438) Add "we won't do this" as a reason to close an issue
- [!17398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17398) Add link to multi-tier sales objection handling
- [!17397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17397) Add mixed-tier objection handling solutions to sales handbook.
- [!17375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17375) Add stage groups to product categories

### 2018-12-18
- [!17484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17484) add link to glossary from sclau section
- [!17480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17480) Fix small typo in follow through section of ux designer handbook page
- [!17473](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17473) Update knowledge-base with Serverless
- [!17471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17471) gerir/infra/blueprint/storage
- [!17460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17460) Change Rent Index to Location Factor
- [!17452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17452) Add Data Quality Process to handbook
- [!17434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17434) Improve workflow documentation for publishing to the blog
- [!17432](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17432) Updates to the Forecasting Process
- [!17378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17378) Adds Viable category maturity
- [!17193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17193) Add Data Quality section
- [!17089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17089) Update Looker to point to Access management process
- [!16831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16831) Resolve "Update sheet load description to actually talk about file naming conventions"
- [!16726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16726) december contributor blog post
- [!16595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16595) Sfdc internal workings branch

### 2018-12-17
- [!17455](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17455) On this page tam changes
- [!17454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17454) Fix eng Secure link and promote Security to General section of hb index
- [!17442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17442) remove sales@ mention
- [!17433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17433) [Communication]: Add a section about asking "is this known"
- [!17425](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17425) Add Manage team repository to team product page
- [!17422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17422) Update notebook specs
- [!17293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17293) adding wbso process
- [!17227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17227) Add review step

### 2018-12-15
- [!17420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17420) change online growth to digital marketing programs
- [!17382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17382) Update source/handbook/marketing/marketing-sales-development/marketing-programs/index.html.md

### 2018-12-14
- [!17413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17413) Propose new workflow for follow through of issues, remove sections that speak...
- [!17410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17410) Update evangelist program page
- [!17393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17393) Update source/handbook/values/index.html.md
- [!17385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17385) Adding DataFox Details
- [!17287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17287) Change file name in the handbook for homepage banner
- [!17278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17278) Add other functionality to Secure
- [!17220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17220) Hackathon page update
- [!17210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17210) Add DevOps stages icons to release posts
- [!17198](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17198) Rename team to stage for release post items
- [!16795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16795) fix broken links for frontend handbook
- [!16640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16640) Adding defining problems before solutions to efficiency values
- [!16639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16639) Add results value of uncertainty
- [!15393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15393) Add design document for Infrastructure Git Workflow

### 2018-12-13
- [!17381](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17381) Update source/handbook/marketing/marketing-sales-development/index.html.md
- [!17364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17364) add smb ownership to table
- [!17357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17357) Add new campaign type details
- [!17356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17356) update timelines
- [!17344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17344) Update Bitergia info.
- [!17333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17333) Replace Internal Review with Stakeholder Collaboration
- [!17329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17329) added on call SLA note
- [!17323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17323) Writers are encouraged to import their blog posts to personal Medium accts
- [!17309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17309) Provide guidance on Product presentations to CAB
- [!17298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17298) Add maximum price for non-Mac laptops
- [!17286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17286) Fix 2 internal pagelinks.
- [!17275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17275) Update users' URL on gitlab.com
- [!17264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17264) Add interning process recommendations
- [!17235](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17235) Add breaking change flow
- [!17201](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17201) Add the second template for  Supporting Community initiative

### 2018-12-12
- [!17328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17328) GDPR Updates
- [!17318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17318) recruiting custom fields
- [!17315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17315) link picture back to original framework image and remove broken links
- [!17313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17313) greenhouse permissions clarify who gets what
- [!17310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17310) Additional language to internal transfers
- [!17304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17304) Beamy how to additions.
- [!17300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17300) Add format to the top of categories.yaml
- [!17261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17261) Clarify social request process
- [!17255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17255) add experience factor worksheet to the promotion process
- [!17215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17215) Update Product Handbook to include internal review of planning and roadmaps
- [!17209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17209) Add personal stories page about remote work
- [!17082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17082) updated internal links that were using JS redirects
- [!16921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16921) Handbook: Support: update issue prioritization

### 2018-12-11
- [!17258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17258) CMM handbook update
- [!17248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17248) Remove accidental left-over word
- [!17244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17244) change online growth to digital marketing programs
- [!17242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17242) greenhouse org changes site admin
- [!17237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17237) Add credit card details to CA handbook
- [!17234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17234) Added new process for MDF credit
- [!17228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17228) Update handbook instructions for handling suspected phishing attempts
- [!17224](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17224) Update title regex for Manage team FEM
- [!17222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17222) Update index.html.md
- [!17206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17206) update field event statuses
- [!17194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17194) Serverless FAQ
- [!17145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17145) Added a section about beta testing
- [!17111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17111) Add knowledge sharing Deep Dive guidelines to handbook
- [!17090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17090) GDPR Article 15 Workflow Updates
- [!17085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17085) Turn category vision suggestion into a markdown template.
- [!16913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16913) canceling future interviews for rejected candidates
- [!16873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16873) adding C1-C3 examples to change requests

### 2018-12-10
- [!17213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17213) Support handbook minor fixes
- [!17204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17204) correct link to offers page
- [!17199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17199) Fixed typo
- [!17187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17187) Update Growth FEM to @dennis
- [!17094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17094) Fix www.about problems
- [!16933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16933) Add more visibility on the growth team
- [!16932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16932) Updating how Sales Segment on Lead is populated
- [!16836](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16836) workflow support: add admin note page
- [!16537](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16537) Adding the additional process of how to mass add contacts and add them as…

### 2018-12-07
- [!17168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17168) add nextravel to tech-stack
- [!17167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17167) Change the speakers to Zendesk
- [!17162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17162) Update merchandise.html.md - Fixed "list of countries we do not do business in"
- [!17159](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17159) modify apac/latam mm
- [!17156](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17156) Add description of team member laptop buyback to offboarding page
- [!17149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17149) modify territory mpas
- [!17148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17148) Update index.html.md
- [!17147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17147) Update index.html.md
- [!17146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17146) Update index.html.md
- [!17142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17142) adding instructions
- [!17127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17127) Fix the broken link to the ceo pricing page
- [!17113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17113) Adds a network architecture diagram
- [!17068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17068) Updating Offboarding Process
- [!16993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16993) Add note to distribution triage steps about follow up issues

### 2018-12-09
- [!17166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17166) Removed Sync areas from Manage "other functionality"

### 2018-12-06
- [!17124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17124) Specify that Delivery does .com releases
- [!17114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17114) Update Coaching section
- [!17098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17098) Whole mumbers
- [!17097](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17097) Details on EE license.
- [!17092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17092) Added the video recording requirement for the Stage Vision page.
- [!17087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17087) Fix broken link & typo
- [!17053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17053) Link terraform automation design doc from index
- [!17046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17046) Added Sync team
- [!17042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17042) Update GitLab Status Twitter account workflow
- [!16877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16877) Added prioritization values to Manage page
- [!16853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16853) Create a new page as a collection of useful links
- [!16805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16805) Quick reference for common workflows

### 2018-12-05
- [!17088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17088) Update mpm handbook with calendar for email marketing
- [!17079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17079) Handbook: support: 2FA add note
- [!17076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17076) move "other security topics" to bottom + add section statement supporting compliance
- [!17073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17073) update how to add fed financial check to bgc
- [!17066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17066) Split Gifs section
- [!17064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17064) Add key to explain product role acronyms
- [!17063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17063) Update youtube/index.html.md to fix typos
- [!17019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17019) Add Product OKRs for 2019 Q1
- [!16910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16910) Adjust office spending caps to better align with market values

### 2018-12-04
- [!17035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17035) update salary field as optional with examples of when to use it
- [!17033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17033) add link to doc, instructions for new access-request, and description of provisioner
- [!17024](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17024) add new source
- [!17018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17018) add speaking sess to scoring
- [!17013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17013) Rename Dev Prod to Eng Prod per industry standards
- [!16985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16985) Adding team members and counterparts
- [!16960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16960) Reverting sync addition to category page
- [!16949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16949) Add on-call participation as reasoning for mobile reimbursement
- [!16916](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16916) Resolve "Add SOW scoping details to CS handbook"
- [!16876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16876) Handle one-license-per-institution EDU rule on a case-by-case basis
- [!16874](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16874) Automatically compress PNG images
- [!16649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16649) Removed sections referring to deprecated Risk Assessment Process
- [!15821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15821) change formatting on blog analysis
- [!15719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15719) Add marketing coordination

### 2018-12-03
- [!17010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17010) Add internal customers section
- [!17000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17000) Fix wrong label mention for blog posts
- [!16999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16999) adding spain and ireland
- [!16997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16997) adding cxc payroll change process
- [!16995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16995) Probationary Periods
- [!16988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16988) Benefits: SafeGuard and CXC
- [!16987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16987) Updated extension for devops-tools workflow page
- [!16935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16935) Improve CA workflow and update CA handbook
- [!16904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16904) Adds sentry deck to support handbook
- [!16897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16897) Update source/handbook/finance/department-structure/index.html.md,…
- [!16883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16883) Newsletter Updates to Handbook

### 2018-12-01
- [!16958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16958) Markdown Layout Modification
- [!16924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16924) Update 404 URL to correct link

### 2018-11-30
- [!16957](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16957) update greenhouse - background checks, contracts, offers
- [!16953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16953) Update source/handbook/marketing/product-marketing/messaging/index.html.md
- [!16950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16950) Reworded reassurance sentence for paging security
- [!16946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16946) sync team to categories page
- [!16941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16941) Update index.html.md to describe our first Support week-in-review iteration
- [!16939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16939) Add suggestions for labeling product categories epics
- [!16928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16928) Update source/handbook/ceo/pricing/index.html.md
- [!16915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16915) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!16914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16914) Create spending company money examples page
- [!16852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16852) Remove Support Turbos & Breach Hawks
- [!16845](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16845) Add reference for important dates.
- [!16803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16803) Add instructions about analyst engagement for product team
- [!16761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16761) Add Growth to product categories
- [!16760](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16760) SMAU introduction.

### 2018-11-29
- [!16906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16906) updated for group conversation livestream
- [!16905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16905) Changed devops-tools.md > devops-tools.html in index
- [!16903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16903) update initial source table
- [!16900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16900) Adding East SDR and fixing 2 spelling errors.html.md
- [!16899](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16899) Update source/handbook/business-ops/index.html.md
- [!16895](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16895) "Quality" is a Department
- [!16887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16887) Add blog templates to handbook
- [!16881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16881) changed SDR Southern Europe to TBH to Anthony Seguillon
- [!16868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16868) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!16858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16858) Moving Security under the Engineering section of the TOC
- [!16835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16835) Add `gitlab-elasticsearch-indexer` to Plan's functionality
- [!16811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16811) Add Technical onboarding (workflows) doc for Secure
- [!16792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16792) Move OSS project to Community Relations subgroup
- [!16782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16782) Workflow support: add account/project change workflow
- [!16765](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16765) Adjusts instructions for updating release post manager

### 2018-11-28
- [!16856](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16856) Bgc 18
- [!16844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16844) update link to 2019
- [!16842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16842) Greenh offers
- [!16838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16838) Added to section on how to request a field event.
- [!16833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16833) Create is the point of contact for shell and workhorse
- [!16832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16832) Include MVC issue and improvement epics in category vision
- [!16826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16826) remove reference to sales@
- [!16822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16822) Add "Team Meetings" calendar instructions
- [!16821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16821) Break 'announcing terminations' into more paragraphs
- [!16818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16818) 401(k) funding process
- [!16816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16816) Added the DevOps Tools page to the community response channels and documented workflow
- [!16812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16812) Improve screenshot compression guide
- [!16810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16810) Added Index and fixed formatting issues
- [!16808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16808) Move Territory info
- [!16807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16807) Updated current object storage state in DR Design Doc
- [!16742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16742) Add "What to work on" section for Create backend team
- [!16715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16715) Remove incentive
- [!16654](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16654) Adding SSO exclusion from prohibition and added examples of removable media reference in the AUP
- [!16286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16286) Outlines different interactions with security
- [!16026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16026) Specify interface from Prioritization (PM) -> Execution (EM)

### 2018-11-27
- [!16798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16798) update new orleans summit invitation letter
- [!16788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16788) Update index.html.md. Add LittleSnitch Firewall as recommended software
- [!16737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16737) Reformat EDU/OSS part of the CA handbook
- [!16700](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16700) Added a section for the Production Change Lock (PCL)
- [!16192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16192) PII workflow
- [!15647](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15647) Terraform automation design doc

### 2018-11-26
- [!16756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16756) update company call to new platform
- [!16750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16750) Update index.html.md to add amendment section
- [!16718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16718) Minor spelling corrections in 360 feedback page
- [!16708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16708) Update Engineering Alignment
- [!16706](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16706) Update Support process for getting an on-call Engineer's attention
- [!16697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16697) Fix some onboarding links after content was moved
- [!16694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16694) create recruiting process framework section, update hiring links

### 2018-11-23
- [!16741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16741) UX Research handbook updates
- [!16717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16717) Handbook: support pages reorg attempt
- [!16671](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16671) Repeat the header for each stage in the maturity page
- [!16601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16601) Document best practices for cross-domain-area collaboration
- [!16502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16502) Add Global Maps & Territory Assignment
- [!16125](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16125) handbook: support workflow dormant process make uptodate

### 2018-11-22
- [!16722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16722) Remove lingering references to CI/CD ops-backend team page
- [!16667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16667) Handbook update for hackathon
- [!16318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16318) added the OPS team will create a separate account in Salesforce instead of can do it ourselves.
- [!16194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16194) add additional inquiry types

### 2018-11-21
- [!16705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16705) Update reseller marketing kit page to include security
- [!16699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16699) Fixing the broken links to Engineering Manager job family
- [!16691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16691) Support handbook deep dive section
- [!16672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16672) Update improving our processes section
- [!16593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16593) Avoid offset meetings
- [!16588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16588) Updated priorities for UX designers
- [!16388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16388) Adding re-instating blocked accounts workflow  fixes https://gitlab.com/gitlab-com/support/support-team-meta/issues/1319

### 2018-11-20
- [!16683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16683) Added missing step to contract instructions.
- [!16681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16681) Update source/handbook/marketing/product-marketing/tiers/index.html.md
- [!16680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16680) Update source/handbook/finance/sales-comp-plan/index.html.md
- [!16678](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16678) included SRE onboirding edits
- [!16676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16676) Update community response channels overview
- [!16670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16670) Fix link to Maintainer/Reviewer
- [!16663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16663) Update index.html.md
- [!16662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16662) updated laptop policy
- [!16658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16658) fixes to Infra main page for Other Pages section
- [!16643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16643) Minor spelling updates to global compensation page
- [!16634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16634) Improve onboarding page.
- [!16633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16633) Bringing the oncall page up-to-date.
- [!16613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16613) Add note about announcing CE/EE settings changes
- [!16582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16582) Added page on feature instrumentation

### 2018-12-08
- [!16666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16666) Add best practice for transitioning 1-1s to new managers

### 2018-11-19
- [!16655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16655) Clarified last step
- [!16653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16653) mention instead of assign
- [!16648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16648) three interviews per day, other scheduling edits
- [!16644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16644) Update source/handbook/finance/department-structure/index.html.md
- [!16622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16622) fix broken images
- [!16579](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16579) Added access control procedures to access management section - access removal timeframe for review by PeopleOps
- [!16510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16510) Define maturity stages
- [!16196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16196) Tam Handbook - Current Upgrading Customers
- [!15714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15714) Update DMCA workflow

### 2018-11-16
- [!16608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16608) add EKS setup file
- [!16602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16602) how to set up email permissions and send on behalf of someone else
- [!16598](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16598) add new campaign channel
- [!16591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16591) Updating CXC
- [!16583](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16583) Update handbook links
- [!16572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16572) Added Acceptable Use Policy Blurb to Code of Conduct page
- [!16571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16571) Local update team page
- [!16570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16570) Update index.html.md revised link to security deck to point to new Nov 2018 deck
- [!16547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16547) Jfinottochange2
- [!16516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16516) Add security to our workflow
- [!16506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16506) Fixing a few more places where FGU is still referenced
- [!16471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16471) Resolve "Add pointing details to the data team page"
- [!16440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16440) Update index.html.md.erb added to solution definition.
- [!16435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16435) Add Combine consistency and agility to Leadership Page
- [!16421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16421) Incident review process
- [!16217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16217) first iteration of Infra OKR blueprint

### 2018-11-15
- [!16576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16576) clarify sharing social links and referrals
- [!16575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16575) Jj pmmhandbook1
- [!16574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16574) Adding AUPolicy Link to Other Resources for Gitlabbers section
- [!16573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16573) ak-pmm-responsibilities
- [!16567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16567) Move pm content
- [!16565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16565) Update index.html.md
- [!16563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16563) ak-add-pr-link
- [!16562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16562) open enrollment
- [!16554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16554) add cigna information
- [!16545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16545) adding thumbs up
- [!16541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16541) Updated Error Budget description
- [!16536](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16536) Update 1:1 index.html.md subordinate -> individual contributor.
- [!16535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16535) fix spelling
- [!16533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16533) Point tech interview link to deep link instead of redirect
- [!16515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16515) fix dormant username links in handbook
- [!16514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16514) wording edits to DMCA workflow
- [!16513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16513) Remove duplicate content between Job Families and Hiring pages
- [!16501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16501) Add images and updates to MPM handbook
- [!16494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16494) Update Product Handbook page to be explicit about dogfooding tracking activities.
- [!16480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16480) Ak split pmm hbook
- [!16457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16457) Restructure planning and roadmaps
- [!16430](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16430) Resolve "Internal channel convention"
- [!16360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16360) Changing "Master" to "Maintainer" in Permissions section
- [!16304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16304) Maintainer mentorship
- [!16281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16281) Add how to become a maintainer for frontend

### 2018-11-14
- [!16508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16508) Integrate Applying page into Jobs FAQ
- [!16505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16505) Low context communications - Handbook update
- [!16504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16504) Update PO process
- [!16503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16503) Create post-interview instructions page
- [!16499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16499) Add experience factor definitions for new backend engineer role
- [!16498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16498) add images
- [!16496](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16496) update vacancy process to include recruiting tasks
- [!16495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16495) Minor nit fixes while reviewing /handbook/product/categories
- [!16492](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16492) Made changes based on discussion with Jamie
- [!16490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16490) Update Manage FE page
- [!16489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16489) Added TOC per handbook style guide
- [!16488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16488) Revert "Update index.html.md"
- [!16486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16486) Updated to include more thorough guide for using ContractWorks
- [!16474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16474) Update suggested email intro template
- [!16463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16463) Cleanup database team page.
- [!16438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16438) Update frontend themed call results and add next theme
- [!16436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16436) Update retrospective
- [!16408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16408) add process for administering secret snowflake
- [!16273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16273) Update Metrics Meeting to include decision and action tracking in the presentation
- [!16243](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16243) move page instructions

### 2018-11-13
- [!16446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16446) Add reference for Kenny Johnston.
- [!16445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16445) Fix TOC on internal AUP page
- [!16443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16443) Adjust Ops Product Leadership
- [!16439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16439) Creation of new AUPolicy Page
- [!16434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16434) Add a section about growing in Transparency to my README
- [!16431](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16431) Fix capitalisation of "Apple Watch" and "Apple Pencil"
- [!16428](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16428) updating candidate information in greenhouse
- [!16419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16419) Update MVP announcement channel to #release-post
- [!15001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15001) Design Doc for Disaster Recovery

### 2018-11-12
- [!16405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16405) Continued work on /handbook/hiring/interviewing
- [!16391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16391) Update MVP process
- [!16382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16382) Values consistency and efficiency

### 2018-11-10
- [!16373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16373) Edit security handbook 2FA guidelines
- [!16371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16371) Adding high-level overview for new delivery team
- [!16370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16370) Update index.html.md

### 2018-11-09
- [!16369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16369) Add team pages for Release and Verify, remove CICD
- [!16366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16366) add public sector contact form to scoring
- [!16362](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16362) Add slack alias to Monitor team page
- [!16359](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16359) Update security best practice 2FA
- [!16357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16357) Add EDW security requirements to Data & Analytics
- [!16356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16356) Jj xdr coaching page
- [!16349](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16349) Provide background for Engineering communication
- [!16340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16340) Merge branch 'update-support-channels' into 'master'
- [!16339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16339) Jj typos3
- [!16334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16334) fix: Update typo

### 2018-11-08
- [!16337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16337) Hiring handbook section rework - pt. 2
- [!16328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16328) Removing PO approval for direct customers since all direct customer POs are null…
- [!16326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16326) Fix #group-conversations Slack channel name
- [!16323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16323) Re-organize /handbook/hiring around target audiences & function
- [!16320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16320) link to Chorus training instructions
- [!16315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16315) Fix broken link
- [!16312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16312) removed extra -
- [!16301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16301) Add implementation details about throughput
- [!16300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16300) Adding new page for Chorus
- [!16299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16299) Adding Chorus to the Tech Stack
- [!16298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16298) clarify List Imports
- [!16296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16296) Fix Licenses page link
- [!16284](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16284) Clarifies the role of Support Team at GitLab
- [!16283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16283) Details how to respond to objections to the technical assignment
- [!16256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16256) Draft a policy for engaging with comments and users on YouTube
- [!16252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16252) Reorganize the sales team's front page in the handbook
- [!16251](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16251) Update on the company expenses
- [!16170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16170) Developer productivity  to be gitlab-docs maintainer
- [!16057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16057) Add merch complaint instructions

### 2018-11-07
- [!16280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16280) Update support team slack channels
- [!16268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16268) Add Plan guidance on picking an issue to work on
- [!16265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16265) removed links to individual sequences, redundant, add Gold trial nurture issue
- [!16263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16263) update bgc policy to include sterling
- [!16262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16262) tos to people ops
- [!16257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16257) Add style for current post
- [!16247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16247) Added note about infrastructure level approval for DB and server-level access
- [!16238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16238) Adding a space - just one
- [!16193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16193) Add throughput labels
- [!16152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16152) clear up messaging on CE/EE vs pricing tiers
- [!16062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16062) Update ee language
- [!15619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15619) Updated UX Research processes

### 2018-11-17
- [!16253](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16253) Support Handbook Combing Updates

### 2018-11-06
- [!16250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16250) add notification config to greenhouse and update offer package process
- [!16245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16245) add Google Search Console and Google AdWords to tech stack
- [!16239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16239) Linked to Sales Process for contracts
- [!16236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16236) Updated slide deck link
- [!16227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16227) Fix Distribution team members
- [!16225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16225) Fix a broken link in the 'Developer productivity team' page
- [!16223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16223) Change ap@gitlab.com to payroll@gitlab.com for contractors
- [!16222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16222) Update index.html.md with gating asset procedure
- [!16218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16218) Update source/handbook/finance/operating-metrics/index.html.md
- [!16212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16212) Changed page title in link
- [!16158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16158) Integrate CEO-101 FAQs into Handbook
- [!15882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15882) Clarifying support policy for short-duration OOO
- [!15628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15628) Add enablement instructions to the handbook
- [!15554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15554) Minor Support Handbook Updates

### 2018-11-05
- [!16209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16209) Removed Group Conversations deck template link
- [!16205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16205) Rename file with proper extension and move to proper folder
- [!16188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16188) Fix link to the patch release template
- [!16157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16157) Add team meeting issue review process to Support Handbook
- [!16122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16122) Move social hb
- [!15735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15735) adding abuse incident section
- [!15702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15702) Added process for contracts
- [!14589](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14589) Add tech writers to product categories

### 2018-11-03
- [!16183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16183) cleanup of infra struct png
- [!16182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16182) infra teams + org structure diagram
- [!16176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16176) Fix PMM pages
- [!16172](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16172) Correct spelling error - Update source/handbook/leadership/index.html.md

### 2018-11-02
- [!16167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16167) Update VP-Group Definitions - source/handbook/leadership/index.html.md
- [!16150](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16150) Update the number of pages in the handbook to 2k
- [!16143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16143) update to ultimate, add links
- [!16133](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16133) Update index.html.md revised competitive app sec page
- [!16132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16132) Update index.html.md revised PMM handbook page
- [!16130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16130) Transpose the maturity data
- [!16129](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16129) Update index.html.md revise competitive marketing page links to app sec
- [!16128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16128) Change exploring to implementing throughput.
- [!16127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16127) Small spelling corrections on the EA index page
- [!16126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16126) over 2000 pages in the handbook!
- [!16124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16124) fix progression order
- [!16096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16096) Junior developers -> junior engineers
- [!16092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16092) Resolve "Update 'how to become a maintainer' to add new guidelines"
- [!16081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16081) Add category epics to product handbook
- [!16015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16015) 2nd iteration on service levels and error budgets framework blueprint
- [!14734](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14734) Add SDR deck

### 2018-11-01
- [!16121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16121) update list import information
- [!16114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16114) Update guidelines for handbook hierarchical structure
- [!16113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16113) Quick markdown formatting fix
- [!16112](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16112) greenhouse additional resources
- [!16105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16105) Updated Manage board links for FE and BE
- [!16100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16100) Add maturity to categories
- [!16094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16094) Update index.html.md removed paragraph explaining difference of roles and…
- [!16093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16093) Update index.html.md moved roles vs persona definition here.
- [!16090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16090) add auto-tag info
- [!16089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16089) Update fix typo
- [!16085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16085) update broken link
- [!16082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16082) Add DMCA workflow reference to security handbook
- [!16078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16078) Update index.html.md removed dates and fixed formatting
- [!16069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16069) Resolve "Update SA Triage process"
- [!15474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15474) add tables of requests
- [!15471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15471) conference policy
- [!14726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14726) Adds a better explanation about inclusive interviewing.

### 2018-11-04
- [!16116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16116) Adds issue #82 report to the research archive

### 2018-10-31
- [!16075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16075) Update source/handbook/marketing/product-marketing/index.html.md
- [!16074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16074) Summit leadership training integration into primary summits page
- [!16072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16072) right link for STD form
- [!16068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16068) Update fix font on report links
- [!16064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16064) update payroll procedures
- [!16061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16061) Update EE language
- [!16058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16058) Reformat Info on the Page
- [!16055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16055) Add link to dependency license pages to distribution handbook
- [!16053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16053) Add next frontend themed call
- [!16051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16051) Consolidate Opportunity information to Business OPS section
- [!16041](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16041) Rearrange handbook handbook table of contents
- [!16038](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16038) Change "remote-only" folder name to "all-remote" and fix related links
- [!16035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16035) Update analyst handbook page to include Thought Leadership section
- [!16009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16009) Test automation career path
- [!16001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16001) Define order of what page to link
- [!15627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15627) Update direct deal PO process

### 2018-10-30
- [!16040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16040) More minor handbook updates
- [!16039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16039) Ensuring people know that don't ask, doesn't mean don't tell for Time off
- [!16036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16036) comp calc may be different after annual review
- [!16033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16033) Add Data Integrity Policy
- [!16018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16018) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!16013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16013) Add info about expensing VPN
- [!16010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16010) blueprint: service levels and error budgets
- [!16007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16007) Still interviewing but not accepting new applicants = Draining
- [!16006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16006) Updating the recruiting alignment to include Kike
- [!16002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16002) Add social native activities
- [!15995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15995) Update _categories.md.erb
- [!15817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15817) Update general guidelines index.html.md into subheadings

### 2018-10-29
- [!16004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16004) links need new target
- [!15990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15990) Stating that medical related time off should also be communicated and moved to STD after 25 days
- [!15988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15988) AmEx policy update.
- [!15986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15986) Quickly removing a comma
- [!15984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15984) Document manager as being the person who manages design tooling licenses
- [!15983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15983) update greenhouse page and vacancy opening paragraph
- [!15976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15976) Editorial style update
- [!15975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15975) Add note on submitting minimal Expensify reports to avoid excessive fees
- [!15966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15966) Link to new issue in Alliances project with template pre-selected
- [!15924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15924) update interviewing page
- [!15865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15865) Resolve "Add information for customer communication to TAM Handbook"
- [!15858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15858) minor handbook support add link to feedback template
- [!15830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15830) Add section on scheduling guideline and code review/submission
- [!15825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15825) Update needs org workflow
- [!15763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15763) support dormant username workflow update
- [!15323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15323) Add design for GitLab CICD.

### 2018-10-28
- [!15958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15958) Add alt links

### 2018-10-27
- [!15952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15952) updating dotcom escalation point
- [!15703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15703) Resolve "Create page for Legal process for Salesforce Negotiations"

### 2018-10-26
- [!15940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15940) Update source/handbook/business-ops/index.html.md to add link to help issue template in _issues.help
- [!15931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15931) Kl update crp
- [!15925](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15925) remove headcount spreadsheet until finalized
- [!15920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15920) reorder tech stack to alphabetical order
- [!15915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15915) iteration on delivery team addition
- [!15911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15911) Add link to slack channel for CS team
- [!15910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15910) Share as much as is useful.
- [!15897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15897) add new campaign types& defs
- [!15896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15896) Use the correct title for Delivery team EM
- [!15894](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15894) Resolve "Move pages to community section"
- [!15891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15891) Resolve "Move feature pages to products"
- [!15888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15888) correction to scoring definitions
- [!15886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15886) Resolve "Move team pages under /company/"
- [!15873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15873) add payroll changes doc
- [!15861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15861) Need to fill out fields in SFDC: Expected Product and Expected Number of User in Stage 1 or Later
- [!15857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15857) Add SPD/Fee Disclosure to the Hanbook
- [!15826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15826) KL_Update source/handbook/marketing/product-marketing/index.html.md
- [!15788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15788) Working on unscheduled issues for Plan team members
- [!15772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15772) minor edits to the support onboarding page
- [!15669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15669) Update Developer role to Backend Engineer
- [!15593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15593) Adding Visual Compliance to the tech stack and also notes around opportunities…
- [!15302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15302) Updated Manage page - team and unscheduled items
- [!15042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15042) add note to carry over not-merged items for kick-off

### 2018-10-25
- [!15883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15883) Molly add issue board
- [!15881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15881) Minor handbook updates to formatting
- [!15880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15880) From led to proposed
- [!15878](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15878) add training link
- [!15872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15872) Security runbook updates
- [!15864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15864) add information on manager input form
- [!15854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15854) Resolve "Move find a speaker page"
- [!15846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15846) Clarify image crediting
- [!15843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15843) compress common links; split mission and vision
- [!15839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15839) Resolve "Install page updates"
- [!15835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15835) Add links to people
- [!15833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15833) Move product categories back to markdown
- [!15832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15832) Resolve "Move Culture section under Company"
- [!15829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15829) Added Boston CISO & CIO event details
- [!15824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15824) update vacancy process in greenhouse
- [!15818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15818) Resolve "Move topic pages to solutions"
- [!15812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15812) updated security handbook page with access process information
- [!15799](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15799) Move product pricing page to CEO folder
- [!15791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15791) Added more info to Data Page in Handbook
- [!15730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15730) updated access request process note on environments page
- [!15590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15590) Fix typos and links
- [!15534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15534) remove outdated info

### 2018-10-24
- [!15823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15823) modify opp close date.
- [!15815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15815) update close date to align w/ sales process
- [!15814](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15814) Added link to Contract approval and procure to pay process
- [!15811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15811) Add Security Awareness Training provider
- [!15808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15808) Offer Signatures to include Recruiting Director
- [!15807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15807) Added word "contract" up higher.
- [!15805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15805) Update source/handbook/marketing/product-marketing/enablement/xdr-coaching/index.html.md
- [!15804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15804) Move instruction for work in progress feature
- [!15803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15803) Add instruction for proper use of the work in progress feature
- [!15797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15797) Update the root cause analysis page filename and links to it
- [!15790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15790) Fix some links in blog handbook
- [!15789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15789) infra/blueprints cleanup
- [!15784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15784) Convert "post-mortem" to "root cause analysis" throughout the site
- [!15782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15782) Add Slack UI tip
- [!15781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15781) Remove areas
- [!15780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15780) Resolve "Update /sdlc links"
- [!15779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15779) fix broken link
- [!15778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15778) Add map for mileage
- [!15776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15776) Resolve "Move licensing FAQ"
- [!15767](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15767) Reformatting 'handbook guidelines' under sub-headings
- [!15755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15755) Make asking for peer reviews a bit more defined by having a minimum stated
- [!15667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15667) Update Exchange Rate policy for clarity
- [!15650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15650) align content marketing team to dev and ops integrated teams
- [!15645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15645) added section about stable counterparts
- [!15610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15610) Gerir/infra/blueprint/delta
- [!15409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15409) Markdown guide: how to embed GitLab Snippets in md files
- [!15278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15278) Add people ops calendar

### 2018-10-23
- [!15775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15775) Update post event process
- [!15764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15764) Update add Gartner ARO page link
- [!15758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15758) Add scheduling info to hb
- [!15757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15757) Fixes a few typos in distribution eng. manager
- [!15756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15756) Update links from categories
- [!15754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15754) Update investigation procedure
- [!15748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15748) Correct the links for EM for Distribution
- [!15736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15736) Resolve "Organize support pages"
- [!15721](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15721) Terminology Update
- [!15666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15666) added postType to definitions and frontmatter
- [!15658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15658) Remove security examples
- [!15591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15591) Update product handbook to utilize design system to work autonomously
- [!15348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15348) Create a team page for Delivery team

### 2018-10-22
- [!15734](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15734) Resolve "Move contact page"
- [!15733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15733) add more greenhouse info, remove lever page
- [!15726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15726) clarify account ownership
- [!15725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15725) added access request directions to tech stack
- [!15724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15724) update IT roles links
- [!15723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15723) Update campaign cost tracking in Marketing Ops page
- [!15722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15722) Add an about page for Marin Jankovski, EM for Distribution and Delivery
- [!15712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15712) Rename functional-group-updates project to group-conversations
- [!15707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15707) Resolve "Move company pages under /company/"
- [!15705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15705) Added finance budget approval
- [!15704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15704) Resolve "Add security to footer nav"
- [!15685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15685) Add async standup description to Monitor team page
- [!15677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15677) Update index.html.md with new MPM Support process for events.
- [!15633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15633) Medium/linkedin handbook update
- [!15631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15631) Event social process update
- [!15622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15622) Make the 'Accepting merge requests' workflow consistent
- [!15515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15515) Resolve "Reconcile stages.yml and capabilities.yml"
- [!15107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15107) Add cross post section to hb
- [!15078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15078) Release post - GitLab 11.4

### 2018-10-20
- [!15697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15697) Resolve "Move cloud image page to handbook"
- [!15696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15696) Add website naming conventions
- [!15692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15692) Add links to main slide files under auto plays. Fix intro paragraph position
- [!15472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15472) Add info about 'Requires e2e tests' and how it is to be used

### 2018-10-19
- [!15690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15690) correct no toc tags
- [!15686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15686) add defs to glossary
- [!15681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15681) fix campaign definitiions
- [!15680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15680) Update list of Security Department resources.
- [!15676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15676) add privacy for public exposure
- [!15675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15675) Update index.html.md
- [!15672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15672) Add off-line demo instructions
- [!15659](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15659) Add Distribution training session DIS007
- [!15655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15655) add video
- [!15653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15653) update details
- [!15640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15640) remove bad quality desk
- [!15637](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15637) Cigna STD
- [!15507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15507) Improved UX workflow graphic to better reflect documented dates
- [!15343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15343) Update link to internal issue tracker for Gold requests
- [!15281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15281) Quality team structure

### 2018-10-18
- [!15644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15644) Move cloud native ecosystem page
- [!15643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15643) Work second.
- [!15642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15642) add identifying quality post section
- [!15639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15639) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!15638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15638) swag updates
- [!15629](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15629) update pre/post-event campaign status deetails
- [!15625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15625) Resolve "Cloud native enablement page"
- [!15621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15621) Use community relations vs. community marketing
- [!15615](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15615) Added definition for monthly active group
- [!15609](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15609) update issue board details
- [!15605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15605) Changing references of WoW to Milestones, adding theme doc and notes about standup and retro
- [!15599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15599) Resolve "Add demo-ready environment reference for SA's"
- [!15596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15596) KL_ Update source/handbook/marketing/product-marketing/index.html.md
- [!15329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15329) - infra planning blueprint
- [!15324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15324) Resolve "Document blog analysis finding in HB"
- [!15298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15298) Resolve "Deprecate /sdlc"
- [!14995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14995) Add team members to the Distribution handbook.
- [!14790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14790) update tables in Production Labels  for Change Review

### 2018-10-21
- [!15635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15635) Scheduling security issues process
- [!15276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15276) Edited link details for sending invoices and executed contracts to ap.

### 2018-10-17
- [!15602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15602) update link to issue board
- [!15594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15594) Adding Visual Compliance to our tech stack
- [!15579](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15579) update the link
- [!15578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15578) add details related to issue board in mktg ops section
- [!15573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15573) Our Listed Initial Sources are the only supported Initial sources via a validation rule
- [!15570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15570) Added payroll email address under Communication section
- [!15568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15568) updated email address for contractor invoices
- [!15563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15563) Corrected the date formats on this page
- [!15559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15559) Fixed broken link for support onboarding issue
- [!15556](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15556) add workflow video
- [!15548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15548) clarifying the need to notify managers of time off.
- [!15537](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15537) Consider tradeoffs of video calls
- [!15535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15535) Added details on instructions for considering a partnership
- [!15504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15504) Cblake xdr roles
- [!15469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15469) Resolve "Move and 301 redirect /comparison/* to /devops-tools/*"
- [!15413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15413) Resolve "Flesh out data page of handbook"

### 2018-10-16
- [!15552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15552) Clarify process for updating Gitlab.com status
- [!15547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15547) debit card
- [!15533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15533) Updates to sales closing process
- [!15530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15530) Remove TriNet
- [!15525](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15525) correct mistype in routing rule
- [!15520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15520) Added planning information to our home page.
- [!15514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15514) Business OPS handbook updated
- [!15509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15509) Updated gemstones description
- [!15460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15460) Reorganize Community Relations handbook
- [!15452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15452) Update ContractWorks instructions
- [!15385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15385) Update Acquisition Process Overview: source/handbook/alliances/acquisition-offer/index.html.md
- [!15246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15246) Changing Benchmarks Based on Role Requirement Changes
- [!14493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14493) Added MAU definition to operating metrics

### 2018-10-15
- [!15506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15506) Update URL for Group Conversations (formerly FGUs) and fix broken links
- [!15503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15503) Named Account Ownership update
- [!15498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15498) Returning swag typo fix
- [!15493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15493) Resolve "Add messaging lead to upcoming release posts"
- [!15490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15490) Add 2018 Timeline for Comp Review
- [!15488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15488) Cblake patch1015 add content to manager section of roles
- [!15483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15483) adjust response time
- [!15480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15480) add eva, fix finance recruiters
- [!15466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15466) Move and elaborate on product vision
- [!15453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15453) add holistic UX to ux handbook
- [!15451](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15451) Update PO requirements for closing deals
- [!15388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15388) FGUs should be renamed to Group Conversations
- [!15361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15361) add 3 no rule with Dan
- [!15352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15352) Add Chef Automation design doc

### 2018-10-14
- [!15461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15461) Expand process for epics

### 2018-10-13
- [!15457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15457) 20181013 Landing page and Handbook updates
- [!15449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15449) rmv dup source
- [!15447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15447) internal control addition -  COA admins
- [!15236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15236) Resolve "Deprecate sdlc.yml"

### 2018-10-12
- [!15445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15445) Update directions for asking handbook questions
- [!15440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15440) Add initial source table to handbook
- [!15437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15437) Update export
- [!15424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15424) add code review tag
- [!15422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15422) HSA Transfer
- [!15418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15418) create recruiting alingment page
- [!15416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15416) Group Conversations to replace FGUs
- [!15414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15414) add best practices
- [!15412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15412) lever to greenhouse for job families and vacancies page
- [!15410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15410) Added "Engaging the Security On-Call" section immediately after the "Security…
- [!15407](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15407) Add Admin Notes process
- [!15406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15406) Updated index.html.md with the Security team on-call section
- [!15405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15405) Rename backend-retrospectives -> async-retrospectives
- [!15397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15397) Mainly fixes link in topic about release posts
- [!15395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15395) updated CW log-in request instructions
- [!15394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15394) Cblake enterprise IT roles page
- [!15366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15366) update how ux work is assigned
- [!15364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15364) KL Update source/handbook/marketing/product-marketing/enablement/index.html.md
- [!15346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15346) Add community response channels workflow sections
- [!15342](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15342) Distribution Open Work Day page
- [!15339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15339) update Company Card Policy

### 2018-10-11
- [!15375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15375) format update
- [!15371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15371) adding example of participating in live broadcast to transparency value
- [!15367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15367) Update reports relevant to XDR enablement
- [!15357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15357) Detail for document process
- [!15356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15356) Add details re zoom license
- [!15355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15355) style consistency: deuppercase headings
- [!15351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15351) Specify "technical debt" as not a "boring" solution
- [!15345](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15345) Auto generate changelog
- [!15338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15338) live demo instructions in toc and change i2p to p2m
- [!15332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15332) Update language to move away from "cost of living," which is misleading
- [!15331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15331) Update Gartner information on other reports
- [!15326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15326) first iteration to GitLab Access Requests Guidelines
- [!15320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15320) Wrong support channel listed
- [!15274](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15274) adding instructions to optimize images
- [!15261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15261) Updates from 2018-10-09 at risk triage
- [!14779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14779) GitLab.com stability update

### 2018-10-10
- [!15328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15328) swag updates
- [!15325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15325) Create a Data and Analytics space in the handbook
- [!15318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15318) Add community response channels overview
- [!15317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15317) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!15316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15316) generalized instructions
- [!15314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15314) Resolve "preparing confidential blog posts"
- [!15313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15313) Add latest overview demo
- [!15310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15310) Added updated links for instructions to ContractWorks.
- [!15309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15309) Improve Create team page  somewhat
- [!15307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15307) Updated to line up with new Contract Approval Workflow
- [!15299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15299) formatting for headers
- [!15293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15293) xdr enablement Enterprise IT Roles 2018-10-10
- [!15290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15290) Update dates and descriptions for Forrester reports
- [!15228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15228) updated Infra FGU responsible person (me)
- [!15202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15202) Enhance Distribution infrastructure maintenance documentation
- [!15182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15182) Added step to "Creating a New Page" section
- [!15000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15000) edits from review apps  to code review and change i2p to p2m.
- [!14931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14931) Update to Lead Routing based on new rules as of 2018-10-01; removing Strategic segmentation.
- [!14191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14191) Generate a changelog for the handbook

### 2018-10-09
- [!15289](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15289) fixing link
- [!15288](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15288) rmv outdated tech
- [!15284](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15284) Update remove old forrester content
- [!15279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15279) Avoid use special characters in branch name that people copy/paste
- [!15268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15268) Update export info in handbook.
- [!15267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15267) rmv tech stack details not used any longer
- [!15263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15263) Chart of Accounts Policy
- [!15255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15255) add medicare notice to benefits page
- [!15249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15249) Add Frontend team pages
- [!15248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15248) Prioritization section for Manage team
- [!15242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15242) Clarify how to add yourself to team page
- [!15241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15241) added oct 2018 newsletter
- [!15240](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15240) updated gemstones
- [!15237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15237) Add information on what to do if someone is working on a ticket that you'd also like to work on
- [!15201](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15201) Fix typo in index.html.md
- [!15178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15178) Resolve "Create Vendor Contract filing process page"
- [!15171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15171) Evangelist program page
- [!15058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15058) Jarv/add canary design

### 2018-10-08
- [!15233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15233) add send limit to outreach info
- [!15227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15227) How to credit blog posts
- [!15222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15222) Clarify criteria
- [!15220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15220) Why business cards for all.
- [!15215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15215) Adding information about how to become an interviewer.
- [!15214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15214) Add LinkedIn
- [!15209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15209) Add License Management report
- [!15203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15203) Fix typo in index.html.md
- [!15191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15191) update company and fgu call instructions
- [!15188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15188) updated TAM index page
- [!15139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15139) Initial CI/CD Infra Blueprint
- [!14841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14841) Add page outlining disaster recovery concepts.

### 2018-10-07
- [!15195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15195) Fix multiple pages w/ spelling formetting and links

### 2018-10-05
- [!15187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15187) Add ToC
- [!15181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15181) Jj xdr coaching
- [!15170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15170) Update handbook - move XDR content to separate dedicated page.
- [!15168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15168) Added instructions on how to request access to YouTube
- [!15166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15166) Update support handbook main page
- [!15165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15165) Update move content from main AR handbook page to this page.
- [!15162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15162) 20181005 mpm section updates
- [!15157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15157) add bizible definitions
- [!15155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15155) Add link to Marketing Programs
- [!15146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15146) Add Bizible definitions and usage
- [!15145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15145) update fgu to change names and add engage
- [!15143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15143) Update fixing some typos, adding dates to reports, adjusting layout
- [!15124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15124) Updated handbook typo
- [!15116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15116) #15 Update wording
- [!15073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15073) Update issue escalations workflow
- [!15064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15064) [Quality] Add a link to the known QA failures
- [!15030](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15030) add blurb around maintaining vision epic/issues up to date.
- [!15015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15015) Added link to vendor contract approval process
- [!14804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14804) Geo Planning Board link update

### 2018-10-04
- [!15141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15141) Resolve "New AR handbook subpage for XDR training"
- [!15132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15132) Clarify inactive issues - blog hb
- [!15131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15131) Remove Forrester VSM info, because link to VSM page is here.
- [!15128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15128) Fix a typo: chang -> change
- [!15126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15126) Add on-call summary description to SRE handbook page
- [!15123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15123) Velocity typo and clarification
- [!15119](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15119) add best practices
- [!15111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15111) Add events calendar
- [!15103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15103) Modify scoring & record creation
- [!15091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15091) Updated status info

### 2018-10-03
- [!15100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15100) fix tool page template and make website edit instructions talk about using it
- [!15092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15092) Adding a "the" as a candidate example.
- [!15081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15081) Add professional services rqst details
- [!15080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15080) Add example of standalone specialist job families
- [!15074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15074) Added link to Manage playlist
- [!15072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15072) Add crosslinks between sections talking about ambition
- [!15067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15067) Updates to CHD
- [!15066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15066) Added "mandatory" comment on details for event to be added
- [!15065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15065) Resolve "add workflow to blog handbook"
- [!15063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15063) add link to email policy in rules of engagement
- [!15061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15061) Link directly to the template
- [!15053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15053) Fix ERB templating in ops-backend/configure/index.html.md.erb
- [!15050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15050) Add email signature to tools-and-tips page
- [!15049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15049) add MPM details to duties chart
- [!15039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15039) Updating  the 360 page to remove Lattice
- [!15021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15021) Update hack day instructions
- [!14976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14976) added detail on trial process
- [!14935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14935) Added security facet in test planning.

### 2018-10-02
- [!15036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15036) add link to issue board
- [!15023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15023) Updating page to include instructions for specialist families
- [!15017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15017) Add blackout notice to handbook
- [!15009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15009) Adjustments to alliances handbook
- [!15005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15005) Update ops-backend/configure/index to match new template
- [!15004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15004) fixed tiny style typo on EJ README
- [!15002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15002) ak-move-press-boiler-plate-section
- [!14997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14997) fix spelling mistakes
- [!14996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14996) Update Plan backend handbook page
- [!14993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14993) Fixing empty alliances handbook page and adding inbound request section
- [!14991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14991) Updated prioritized board link for Manage
- [!14984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14984) fix dalia's role and update monitor team name
- [!14982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14982) update blog handbook
- [!14946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14946) Add quality team’s project management process
- [!14753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14753) Capabilities are a grab bag.
- [!14652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14652) updated wording for term departures

### 2018-10-01
- [!14971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14971) Clarify instructions for adding features
- [!14962](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14962) Fix a few typos in source/handbook/engineering/quality/test-engineering/index.html.md
- [!14960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14960) Add team members to Monitor team page
- [!14956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14956) Update handbook for Content Hack Day Q3
- [!14953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14953) Build individual team pages using data from team.yml
- [!14947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14947) Tiny grammar fix turned reword for meetup schedule
- [!14943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14943) Add link to dir readme and update CI/CD names
- [!14940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14940) Template for team pages
- [!14938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14938) RACI in README
- [!14933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14933) Adding the MSA or EULA effective date if the client/prospect will not sign our order form.
- [!14907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14907) updated Terminus section and swag.
- [!14789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14789) Update blog handbook
- [!14303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14303) Fix link + better description for adding blog posts social sharing image

### 2018-09-30
- [!14937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14937) Management span.
- [!14936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14936) Emphasizing velocity
- [!14926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14926) 20180929 mpm section updates

### 2018-09-29
- [!14912](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14912) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14911) fix URLs without titles
- [!14707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14707) Clarify that GitLab Gold is available for private accounts
- [!14617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14617) Added test engineering, plans and process to the Quality handbook

### 2018-09-28
- [!14871](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14871) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14867](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14867) Update swag requests
- [!14862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14862) Remove arrows from Forrester graphics
- [!14858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14858) Update source/handbook/tools-and-tips/index.html.md
- [!14857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14857) Update source/handbook/marketing/product-marketing/competitive/application-security/index.html.md
- [!14852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14852) Add CFO to approval process
- [!14844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14844) Resolve "Correcting a grammar mistake in the Gitlab handbook."
- [!14813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14813) Adding an alliances handbook
- [!14808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14808) Updates to engagement rules and adding MQLs
- [!14580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14580) clarifying spending money
- [!14265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14265) adds gdpr article 15 workflow
- [!14027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14027) Create "How to Respond to Tickets" Page

### 2018-09-27
- [!14859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14859) Added detail around "value" to the person.
- [!14851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14851) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14850](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14850) Updated Email Communication Policy section
- [!14837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14837) Clarifying that if no level is indicated, it is intermediate
- [!14833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14833) Update Forrester list to remove CI tools. It's above.
- [!14830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14830) Update remove ARO from future reports
- [!14824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14824) Update source/handbook/marketing/marketing-sales-development/sdr/index.html.md
- [!14822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14822) Wiring Design to Infra main page.
- [!14817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14817) Add notes about ambitious planning to product handbook and direction page
- [!14816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14816) Fix "Geo development" link
- [!14807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14807) Update to include lessons learned in handbook
- [!14805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14805) update to pro rated language in director comp
- [!14796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14796) infra/5088: storage nodes design
- [!14784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14784) initial infra design and template
- [!14296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14296) Fix broken link to upgrade metrics on distribution page

### 2018-09-26
- [!14788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14788) Move content marketing to corporate marketing
- [!14777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14777) Reword Create team description and link to 2019 product vision.
- [!14775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14775) Adds two extra points to the how to become a maintainer guide
- [!14774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14774) Remove step to unlabel from Distribution triage documentation
- [!14773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14773) Update post event f/up to match process
- [!14770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14770) Links updates and change to APAC call time
- [!14768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14768) Dalia's readme
- [!14733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14733) example to clarify stock options for promotion
- [!14608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14608) Add 'team calendar' section to monitoring team page

### 2018-09-25
- [!14757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14757) Fix CI/CD typos
- [!14754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14754) Adjusting the language for transfers to add clarity
- [!14743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14743) Update source/handbook/git-page-update/index.html.md
- [!14740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14740) GDPR Workflow Fixes
- [!14730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14730) Update handbook with training docs from week 1
- [!14723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14723) Update ops backend configure team page
- [!14714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14714) Fix links which describe how we decide what is paid and what tier
- [!14713](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14713) Add GCP Security Guidelines Policy link to internal doc in handbook
- [!14712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14712) Add security-aware dev report to archive
- [!14673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14673) Rename Community Marketing to Community Relations in documentation
- [!14667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14667) Fix minor typo on Communication page

### 2018-09-24
- [!14704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14704) Add dev research report to archive
- [!14702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14702) Example for candidate on how to edit the handbook
- [!14694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14694) Move Secret Snowflake page to the People Operations section
- [!14692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14692) additional wording to internal transfers.
- [!14688](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14688) Update editing paragraph to get rid of mailto
- [!14687](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14687) Update training paragraph.
- [!14686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14686) Clarify role of backups
- [!14681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14681) add results from survey page
- [!14676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14676) Metric Meeting Format
- [!14675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14675) Update segmentation in Business Ops Section
- [!14672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14672) Adds GitLab instance survey report under "GitLab.com"
- [!14642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14642) Add Bora as the OSS Project expert
- [!14614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14614) Adding HRBP to review discretionary bonus
- [!14558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14558) Update index.html.md
- [!14402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14402) Update EDU process

### 2018-09-22
- [!14660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14660) add demo link
- [!14656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14656) Minimize pressure to work long hours
- [!13955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13955) GitLab 11.3 Release Post

### 2018-09-23
- [!14653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14653) Update source/handbook/finance/operating-metrics/index.html.md

### 2018-09-21
- [!14649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14649) Added logo specifications to Design System
- [!14644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14644) Update the email request process
- [!14633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14633) Add EMEA Support / Sales / CS cross functional meeting
- [!14627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14627) Fix broken link for Jetbrains license management in "Spending Company Money" section
- [!14625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14625) documenting secret managemnent
- [!14621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14621) Add Security Products header and anchor
- [!14618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14618) Infrastructure Blueprint
- [!14607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14607) Rollover to Betterment

### 2018-09-20
- [!14601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14601) non-reimbursement expenses
- [!14600](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14600) Update index.html.md.
- [!14595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14595) Add support and docs cross functional meeting
- [!14590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14590) Rename engineering_areas to capabilities
- [!14586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14586) Clarify experience factor template team member input section
- [!14565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14565) adds cross functional meetings
- [!14529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14529) Update:  Adding Abuse team to workflow
- [!14433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14433) Move Monitor team sections around, remove technology
- [!14403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14403) Changes to the Geo Team frontpage

### 2018-09-19
- [!14564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14564) contractor invoice
- [!14562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14562) add new slack channel
- [!14560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14560) Add deprecaed doc to the sales enablement training page on PMM part of website
- [!14551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14551) Add community contributions to product priority
- [!14546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14546) Update fixing errors by deleting section
- [!14533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14533) Fix playlist link
- [!14527](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14527) Update Marketing Handbook links
- [!14523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14523) Fix several issues with communication page.
- [!14517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14517) Removed link to article that doesn't exist anymore
- [!14502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14502) Flow one
- [!14498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14498) handbook: support: add chair, internal requests
- [!14479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14479) Define the difference between "team leads" and "managers" at GitLab
- [!14449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14449) Added Vendor Contract process

### 2018-09-18
- [!14508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14508) Update analyst handbook page with training
- [!14507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14507) add links to pipe-to-spend links to Update source/handbook/business-ops/index.html.md
- [!14500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14500) entire devops lifecycle
- [!14495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14495) Warn about Slack private groups
- [!14486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14486) fix broken greenhouse link
- [!14484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14484) remove referral, update greenhouse with zoom, removed sections in lever already in gh
- [!14482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14482) Remove Robert from the Quality team page
- [!14478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14478) Update update with education section of analyst reports
- [!14476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14476) add domestic partner hsa information
- [!14469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14469) Removing AZ
- [!14467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14467) Define EULA Acronym
- [!14466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14466) Add TOC.
- [!14465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14465) Adding Outreach.io Call disposition info to handbook
- [!14453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14453) Entire DevOps Lifecycle and 200 percent messaging
- [!14452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14452) Secure Team - Add YouTube playlist
- [!14450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14450) Jj removing maturitymodel
- [!14448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14448) AR Workflow for refunds
- [!14442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14442) Move redirect setup policy to the handbook-usage page
- [!14333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14333) Fix link to image, and make Markdown source conform more to standards.
- [!14312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14312) Explain why we still use "Security Products"
- [!14204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14204) Fixing a typo in the handbook
- [!14190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14190) Update the number of contributors to GitLab CE
- [!14128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14128) Add bullet point in Communication linking to Chat channel page
- [!14037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14037) Update "hiring SOs/family members" policy to address concerns of privacy

### 2018-09-17
- [!14446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14446) Add social media profile assets
- [!14441](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14441) Replacing growth marketing with pipe-to-spend
- [!14440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14440) Fix broken links to the /hiring/roles/ folder
- [!14439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14439) Fix broken link to success_image
- [!14438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14438) added sales enablement
- [!14435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14435) Add new experience factor worksheet with soft skills, team member inputs
- [!14423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14423) more updates to reseller marketing kit
- [!14420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14420) KL Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14412) add inmail issue template Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!14411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14411) Cigna ID Cards
- [!14409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14409) Revert "Merge branch 'complete-devops-lifecycle' into 'master'"
- [!14404](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14404) PS Opps must have a Parent Opp
- [!14400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14400) Reviewers and Maintainers process
- [!14394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14394) Add instructions for tool logos on home and compare pages
- [!14391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14391) swag updates
- [!14387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14387) move capabilities from pricing page
- [!14382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14382) Update handbook pages via new community contribution workflow
- [!14354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14354) Updating Barbie's schedule Hours preferences under EA
- [!14344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14344) Move Secure page to direction
- [!14340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14340) Add theme for Ocotober frontend call
- [!14316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14316) Flatten categories and capabilities to make it easier to communicate and reason about.
- [!14197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14197) Add advocate-for-a-day Slack channel to CM handbook

### 2018-09-16
- [!14390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14390) moving some swag stuff to just be in corporate marketing
- [!14388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14388) Update source/handbook/marketing/product-marketing/reseller-kit/index.html.md
- [!14384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14384) Resolve "Comparison PDF Generation is Broken"
- [!14334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14334) Update mentions of Complete Devops

### 2018-09-15
- [!14380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14380) Add notice about adding links to categories and capabilities
- [!14377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14377) Add DevOps to "Is it similar to GitHub?" message
- [!14352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14352) Added note about 'partially' flag to 'tools' field.

### 2018-09-14
- [!14367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14367) 401k Information
- [!14361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14361) Standardize on complete DevOps lifecycle
- [!14330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14330) Link into Greenhouse information, remove more references to Lever
- [!14323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14323) Add pipe-to-spend to Marketing Metrics section in source/handbook/business-ops/index.html.md
- [!14317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14317) Update content and links to reseller marketing kit
- [!14214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14214) Make it simpler
- [!14207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14207) Replacing availability with Team Meetings Calendar.
- [!14161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14161) Update meltano team roles

### 1970-01-01
- [!14331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14331) Correct "setup" and "set up" grammar sitewide
- [!13958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13958) Fix typo in confidential information section

### 2018-09-13
- [!14301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14301) Update Exec preferences
- [!14298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14298) Minor Manage Team page update
- [!14290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14290) added manual way process for Non ACH and Non US Residents
- [!14287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14287) fix image
- [!14283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14283) moved swag from fm
- [!14282](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14282) moved swag to corp marketing
- [!14279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14279) Revert note about registry not working with canary env
- [!14273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14273) Adding notes that Parent account is the Ultimate Parent Account in SFDC
- [!14266](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14266) Add devops interview report to archive
- [!14262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14262) Update MVC section with thin content details
- [!14242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14242) Resolve "Update categories to use stub vs name"
- [!14212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14212) Support workflow: needs-org: add resync
- [!14209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14209) remove link written over
- [!14152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14152) Update Supporting Community Individuals CA handbook
- [!14138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14138) support workflow: update/rename 2FA
- [!14023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14023) Creating reseller marketing kit page
- [!13996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13996) handbook: add link to support email workflow

### 2018-09-12
- [!14261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14261) Changing tone
- [!14258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14258) Add link to Tommy's README
- [!14250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14250) Remove hiring order list
- [!14244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14244) comparison summary instructions
- [!14241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14241) editing XDR coaching
- [!14236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14236) Fix services .com support bootcamp link
- [!14234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14234) fix headers on greenhouse page
- [!14229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14229) adding XDR coaching to the enablement page
- [!14223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14223) Update source/handbook/engineering/career-development/index.html.md
- [!14216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14216) Add partial reimbursement language
- [!14203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14203) Update field marketing page
- [!14179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14179) Specifying confidentiality rule for security issues
- [!14137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14137) adds information request workflow
- [!14127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14127) Add new structure for Monitoring Team handbook page

### 2018-09-11
- [!14218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14218) featured blog post instructions
- [!14211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14211) Eric's Links
- [!14208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14208) Servant leader
- [!14194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14194) update referral info for GH
- [!14193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14193) Updates to Benefits with Lumity
- [!14184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14184) Never too high, or too low
- [!14181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14181) clarify website scope and ownership
- [!14180](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14180) Clarifying non-traditional background
- [!14151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14151) Update source/handbook/engineering/infrastructure/production/index.html.md
- [!14079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14079) Added some additional language to the initiative part.  Managers and employees…

### 2018-09-10
- [!14175](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14175) clarify prime commuting times
- [!14168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14168) Greenhouse-commits
- [!14146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14146) Added retro info to Manage page
- [!14145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14145) Update zoom info
- [!14143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14143) Added Functional Approval defintion
- [!14130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14130) Add sre report to ux archive
- [!14090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14090) Update security paradigm link
- [!14062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14062) Update source/handbook/engineering/career-development/index.html.md

### 2018-09-07
- [!14135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14135) Add Lumity Payroll Processes
- [!14118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14118) Documenting Iconography as part of our Marketing Design System
- [!14105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14105) Added interviews with churned users report
- [!14098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14098) Fix a typo on the Quality page
- [!14086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14086) adding some language around career development roles and responsibilities for…
- [!14008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14008) Offboarding Procedures: updating for clarity

### 2018-09-06
- [!14100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14100) Update index.html.md
- [!14082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14082) Updates to monitoring page
- [!14080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14080) Resolve "move case study publishing process to customer reference handbook"
- [!14078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14078) Rename Product Core Values to Product Principles so it's not confused with company values
- [!14073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14073) update the campaign member status
- [!14071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14071) Fixes various links for the public dashboards
- [!14070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14070) Fix broken arch image and atom is opionated about whitespace/punctuation.
- [!14069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14069) Add note on manually scheduling coffee breaks. Improve instructions to add new team members via the Web IDE.
- [!14057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14057) Note how to request an account on Staging. There has been multiple Slack…
- [!14055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14055) Rename Security Features page to Security Paradigm
- [!14046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14046) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14044) Add social takeover notes
- [!14042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14042) Update process to be more accurate
- [!13975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13975) Add security features page

### 2018-09-05
- [!14043](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14043) Fix small typo in source/handbook/engineering/career-development/index.html.md
- [!14040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14040) crosslink campaign expense policy to finance handbook
- [!14039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14039) deprecate spreadsheet
- [!14031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14031) Update source/handbook/values/index.html.md
- [!14022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14022) reupdate company call
- [!14018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14018) Add gitlab-ce#48847 to UX Design archive
- [!14012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14012) Add section on career coaching for engineering, senior dev template
- [!14006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14006) Career Dev components
- [!14004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14004) Eric readme
- [!14003](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14003) Add github ci cd faq
- [!14000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14000) corrected run-on sentence on spending company money
- [!13999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13999) Adds research report to research archive
- [!13998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13998) Add new depts and allocation methodology
- [!13990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13990) Typo
- [!13970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13970) Fix links to dashboard graphs

### 2018-09-04
- [!13972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13972) Rename eShares to Carta
- [!13971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13971) Pension Edits

### 2018-09-03
- [!13960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13960) Clarify Sales OPS link to BizOPS