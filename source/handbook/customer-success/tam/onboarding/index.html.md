---
layout: markdown_page
title: "Account Onboarding"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## Technical Account Manager Handbook Pages

- Account Onboarding *(Current)*
- [Technical Account Manager Summary](https://about.gitlab.com/handbook/customer-success/tam/)
- [Account Triage](https://about.gitlab.com/handbook/customer-success/tam/triage)
- [Account Engagement](https://about.gitlab.com/handbook/customer-success/tam/engagement)

---

# Customer Onboarding

We should include a pre-defined set of professional services in each initial purchase. This aims to improve the customer onboarding experience across for all customers whilst setting a precedent to how all customers are managed (with regards to outreach, support, technical account management etc.) throughout the duration of their experience and relationship with GitLab. 

Technical Account Managers and Implementation Engineers should work closely together throughout the onboarding process, with support from Solutions Architects and Strategic Account Leaders/Account Managers where appropriate.

Please note that these gemstones are NOT intended to replace sales segmentation, they are a way of categorising customers in a way that's relevant to Customer Success.

Below is an outline of  the ideal onboarding experiences that we want to deliver for our customers based on deal size. This is intended to be included as part of the customer's GitLab purchase at some point in either Q1 or Q2 2019 (TBD) once we have enough headcount to deliver it. Expectations MUST be managed with the customer early on that anything outside of these strict boundaries must have a Statement of Work attached and be paid for before any of the additional work is delivered.

Customer onboarding is a 45 day time period. All implementation and training must be scheduled to be delivered within the first 30 days of purchase. The Technical Account Manager is responsible for ensuring that this happens.

## Diamond
Accounts categorized into Diamond are all accounts that are greater than 5000 users, OR greater than 2000 users AND using Ultimate/Gold, OR greater than $500,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager  | Implementation Engineer  |
| 60 minute weekly call with TAM and IE  | Two weeks on-site/remote support - Must be scheduled within 30 days of purchase  |
| GitLab Customer Success project  | Four one-hour meetings focused around HA implementation support & HA training certification (5-10 attendees) - Must be scheduled within the first 30 days of purchase |
| Personal outreach & automated outreach  |  GitLab Administration Training & up to three additional training courses of their choice, including train-the-trainer certification if desired. - Must be scheduled within the first 30 days of purchase   |

## Pearl
Accounts categorized into Pearl are all accounts that are not Diamond AND greater than 2500 users, OR greater than 1000 users AND using Ultimate/Gold, OR greater than $250,000 in TAV (Total Account Value).*

| Technical Account Management | Implementation |
|---|---|
| Dedicated Technical Account Manager | Implementation Engineer |
| 30 minute weekly call with TAM and IE  | One weeks on-site/remote support - must be scheduled within 30 days of purchase  |
| GitLab Customer Success project  | Four one-hour meetings focused around HA implementation support & HA training certification (1-5 attendees) - Must be scheduled within the first 30 days of purchase |
| Personal outreach & automated outreach | Up to two training courses of their choice, including train-the-trainer certification if desired. - Must be scheduled within the first 30 days of purchase |

## Sapphire
Accounts categorised into Sapphire are all accounts that are not Diamond or Pearl AND greater than 1000 users, OR using Ultimate/Premium/Gold/Silver AND greater than $100,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation |
|---|---|
| Dedicated Technical Account Manager | Implementation Engineer  |
| 30 minute bi-weekly call with TAM and IE  |  One week of remote support - must be scheduled within 30 days of purchase |
| GitLab Customer Success project  | Four one-hour meetings focused around HA implementation support & HA training certification (1-3 attendees) - Must be scheduled within the first 30 days of purchase |
| Personal outreach & automated outreach | One training course of their choice - Must be scheduled within the first 30 days of purchase |

## Ruby
Accounts categorised into Sapphire are all accounts that are not Diamond, Pearl or Sapphire AND greater than 500 users, OR greater than $50,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager  (Growth focused - smaller accounts)  |  Implementation Engineer  |
| Monthly calls with TAM and IE  | 5 hours of remote implementation support - Must be scheduled within the first 30 days of purchase |
| GitLab Customer Success project  | GitLab 101 Training Course - Must be scheduled within the first 30 days of purchase |
| Personal outreach & automated outreach  |   |

## Quartz
Accounts categorised into Quartz are all accounts left over after Diamond, Pearl, Sapphire and Ruby have been categorised.

* Account Manager (?)
* Automated outreach
* No Implementation included

**Total Account Value is calculated by adding together the current year’s ARR opportunity and all future renewal opportunity amounts together.*


# Customer Records and Profiling

## The Customer Meta-Record
The concept of a customer meta-record is how a Technical Account Manager develops and maintains a holistic understanding of customer accounts.  It is a living record that includes both business and technical information about the customer.  The data captured here informs not only the Technical Account Manager, but all of GitLab about critical details to the success of our customers. 

## Business Profile
The business profile is captured during the discovery and scoping stages in the pre-sales phase of the customner lifecycle.  Once an opportunity is set to closed-won, the Technical Account Manager is largely responsible for maintaining this data. The business profile of a customer's meta-record contains data points related to their organizational structure, the internal advocate/champion, the features most compelling to the customer, the "why" of purchasing GitLab, objectives for the implementation and critically, the data captured in the customer feedback loop.

## Technical Profile
The technical profile includes objective data points about a customers technical landscape and can be captured at any stage of the customer lifecycle. Specifically, items related to architecture, sizing, scale, security and compliance requirements are captured in the technical profile. In general, solution architects and implementation specialists are primarily responsible for collecting and capturing information for the technical profile. This information is transitioned to the Technical Account Manager once a customer goes live and the Technical Account Manager maintains the information throughout the remainder of the customer lifecycle.

## Automated Customer Discovery Tools
A series of data sheets and automation scripts is being developed that will streamline much of the intake, discovery and reconnaissance activities during the pre-sales stages of the customer lifecycle.  Decisions surrounding where this data will reside and which information is part of the critical path during customer onboarding are currently underway.

Currently, These profiles and data points are a work in progress and the Customer Success Department are trying and testing how to efficiently do this using various different tools and methods. 

Some of these currently include:  

* The Customer Collaboration Project - we are testing this as a type of customer success plan as well as a collaboration project between the team assigned to the customer.
* Google documents, such as decks (a slightly older method) which outline a success plan created by the Strategic Account Leader and running notes pages within "Pods", where account planning is reviewed each week/bi-weekly between those pods.
* Salesforce (SFDC): This is our main source of truth and contains all activity, notes and communication between Technical Account Managers, Solutions Architects and Implementation Engineers and their respective customers. The Customer Success Plans should also be linked to this record.

We are currently researching various Customer Success Applications, such as Gainsight, with which to fully manage post-sales customer success, reporting and strategy around accounts. Salesforce is somewhat limited in certain areas that pertain to Customer Success so this is becoming more of a necessity and will hopefully become part of our workflow some time in 2019.
