---
layout: markdown_page
title: "Create Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Create Team
{: #create}

The Create team is responsible for all backend aspects of the [Create stage of the DevOps lifecycle][stage],
which is all about creating code, and includes all functionality around source code management,
code review (merge requests), the Web IDE, wikis, and snippets.

For more details about the vision for this area of the product, please
see the [Create stage][stage] page and the [2019 product vision blog post][blog post].

[stage]: /handbook/product/categories/#create
[blog post]: /2018/09/21/create-vision/

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Create') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /, Create/, direct_manager_role: 'Engineering Manager, Create') %>

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create team, it's best to [create an issue] in the relevant project
(typically [GitLab CE]) and add the ~Create label, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create] on Slack.

[engineering workflow]: /handbook/engineering/workflow/
[create an issue]: /handbook/communication/#everything-starts-with-an-issue
[GitLab CE]: https://gitlab.com/gitlab-org/gitlab-ce

### What to work on

The primary source for things to work on is the [Create issue board] for the 
current release (don't forget to filter by milestone!), which is compiled by
the Product Manager following the [product prioritization process], with input
from the Backend and Frontend Engineering Managers and other stakeholders.

#### What to work on first

Issues labeled P1 and P2 are considered top priority and are expected to be 
done by the [feature freeze on the 7th] so that they can make it into the 
intended release. 
The total weight of these issues is limited to the expected total weight the 
team will be able to complete, based on historical performance and developer 
availability. 
In the future, we may start using [throughput] to determine a reasonable amount
of work instead.

These top priority issues will be assigned to developers on or ahead of the 8th
of the month, and it is their responsibility to make a best effort to get them
done by the feature freeze, and to inform their engineering manager if anything
is standing in the way of their success. 
There are many things that can happen during a month that may result in a P1 or
P2 deliverable not being done by the 7th, but this should never come as a 
surprise, and the sooner this potential outcome is anticipated and communicated,
the more time there is to see if anything can be done to prevent that outcome, 
like reducing the scope or finding a different developer who may have more time
to finish the issue.

Generally, your assigned P1 and P2 issues are expected to take up about 75% of
the month. 
The other 25% is set aside for other responsibilities (code review, community
merge request coaching, [helping people out in Slack, participating in 
discussions in issues][collaboration], etc), as well as urgent issues that come
up during the month and need someone working on them immediately (regressions, 
security issues, customer issues, etc).

#### What to work on next

If you have time to spare after finishing your top priority issues and other 
activities, you can spend the remaining time working on P3 and P4 issues. 

P3 and P4 issues are usually not directly assigned to people, except in cases
where a specific developer is the obvious most appropriate person to work on it,
if it's technical debt or a bug related to a feature they built very recently, 
for example. 
These issues are not expected to be done by the feature freeze but will become
P1 and P2 issues in the next release, so any work done on them ahead of time is
a bonus.

Instead of working on P3 and P4 issues, you may also choose to spend any spare
time working on anything else that you believe will have a significant positive
impact on the product or the company in general. 
As the [general guidelines] state, "we recognize that inspiration is 
perishable, so if you’re enthusiastic about something that generates great 
results in relatively little time feel free to work on that." 

We expect people to be [managers of one][efficiency] and prefer [responsibility
over rigidity][efficiency], so there's no need to ask for permission if you 
decide to work on something that's not on the issue board, but please keep your
other responsibilities in mind, and make sure that there is an issue, you are
assigned to it, and consider sharing it in [#g_create].

[Create issue board]: https://gitlab.com/groups/gitlab-org/-/boards/363876?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Create&label_name[]=backend&label_name[]=Deliverable
[product prioritization process]: /handbook/product/#prioritization
[feature freeze on the 7th]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd
[throughput]: /handbook/engineering/management/throughput/
[collaboration]: /handbook/values/#collaboration
[general guidelines]: /handbook/general-guidelines/
[efficiency]: /handbook/values/#efficiency

### Retrospectives

The Create team conducts [monthly retrospectives in GitLab issues][retros]. These include
the backend team, plus any people from frontend, UX, and PM who have worked with
that team during the release being retrospected.

These are confidential during the initial discussion, then made public in time
for each month's [GitLab retrospective]. For more information, see [team
retrospectives].

[#g_create]: https://gitlab.slack.com/archives/g_create
[retros]: https://gitlab.com/gl-retrospectives/create/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[GitLab retrospective]: /handbook/engineering/workflow/#retrospective
[team retrospectives]: /handbook/engineering/management/team-retrospectives/
