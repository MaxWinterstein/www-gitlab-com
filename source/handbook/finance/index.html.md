---
layout: markdown_page
title: "Finance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Communication
<a name="reach-finance"></a>

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/finance/issues/)- please use confidential issues for topics that should only be visible to team members at GitLab.

- [**Chat channel**](https://gitlab.slack.com/archives/finance)- please use the `#finance` chat channel in Slack for questions that don't seem appropriate for the issue tracker or internal email correspondence.

- **Accounts Payable**- inquiries which relate to vendor and merchant invoices should be sent to our Accounts Payable mailbox - ap@gitlab.com. Of course, electronic copies of all invoices should be sent to this address upon receipt.

- **Accounts Receivable**- customer billing inquiries should be sent to our Accounts Receivable mailbox – ar@gitlab.com

- **Payroll**- inquiries which relate to contractor invoices should be sent to our Payroll mailbox - payroll@gitlab.com.

## Other Pages Related to Accounting and Finance

- [Signature authorization matrix](/handbook/finance/authorization-matrix)
- [Contract and Payment Approval Process](/handbook/finance/procure-to-pay)
- [Sales compensation plan](/handbook/finance/sales-comp-plan)
- [Analytics](/handbook/finance/analytics)
- [Operating Metrics](/handbook/finance/operating-metrics/)
- [Monthly Budget vs Actuals Review Process](/handbook/finance/monthly-budget-actuals-review/)
- [Stock Options](/handbook/stock-options/)
- [Travel and Expense Guidelines](/handbook/finance/travel-expense-guidelines)
- [Tech Stack](/handbook/business-ops/#tech-stack)
- [Financial Planning Process](/handbook/finance/financial-planning-process/)

## General Topics


### Legal and Financial Information

For commonly requested company information, please visit our [wiki page](https://gitlab.com/gitlab-com/finance/wikis/company-information).

Forms
* [Link to W9 Doc](https://drive.google.com/file/d/0B-ytP5bMib9TcEE3WmppMUR6WFVTVHhNSnhWWGZjdzVqM29N/view)
* [Link to form CA-590](https://drive.google.com/a/gitlab.com/file/d/0BzE3Rq8kSQ6Tcmp3a19xcFBZOWs/view?usp=sharing)

### Company Accounts
<a name="company-accounts"></a>

Login information for the following accounts can be found in the Secretarial vault
on 1Password:

- FedEx
- Amazon
- IND (Immigratie en Naturalisatie Dienst, in the Netherlands) company number

If you need this information but cannot find it in your shared vaults, check with PeopleOps to get access.


## Invoice template and where to send
<a name="invoices"></a>

Vendor invoices are to be sent to ap@gitlab.com and payroll@gitlab.com for contractors. An [invoice
template](https://docs.google.com/spreadsheets/d/1sRA2uCpFblOleyVIslqM4YwbW27GkU5DTgwMLhgR_Iw/edit?usp=sharing) can be found in Google Docs by the name of "Invoice Template".

Contractors can send the invoice around the middle of the month to ensure payment by the end of it. Include any [expenses](/handbook/spending-company-money/) in the invoice.

In many cases, VAT will not be payable on transactions between GitLab BV and EU-based
vendors/contractors, thanks to "Shifted VAT". To make use of this shifted VAT:

* The vendor/contractor writes the phrase "VAT shifted to recipient according to
article 44 and 196 of the European VAT Directive" on the invoice along with the
VAT-number of GitLab BV (NL853740343B01).
* On the vendor's VAT return the revenue from GitLab BV goes to the rubric "Revenue within the EU". It goes without saying that vendors are responsible for their own correct and timely filings.
* GitLab BV files the VAT on the VAT return, and is generally able to deduct this VAT all as part of the same return.

### Timesheets for Hourly Employees

1. People Ops and Finance will share a private Google Sheet with you where you will log your hours for each day in the “hours” column.
1. There is a dropdown in the “pay type” column, with the default being Regular. There are also options for Overtime, Vacation, Sick, and Bereavement. Choose the appropriate pay type for your time.
1. If you work overtime or more hours than agreed upon in your contract, please obtain approval from your manager and forward to Finance before payroll cutoff.
1. Your timesheet is due one day prior to the submit payroll date, which is outlined for the calendar year on your timesheet.

## Finance Team Policies and Procedures

- [Accounting and Finance Policies](/handbook/finance/accounting-and-finance-policies/)
- [Accounting and Finance Procedures](/handbook/finance/accounting-and-finance-procedures/)

### Department Information
- [Department Structure](/handbook/finance/department-structure/)
- [Creating New Departments](/handbook/finance/creating-new-departments/)
