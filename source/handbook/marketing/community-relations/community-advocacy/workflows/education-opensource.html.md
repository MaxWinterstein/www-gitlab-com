---
layout: markdown_page
title: "Education and Open Source"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Description of the workflows when handling Gold/Ultimate license requests for the Education or Open Source initiatives.

## Workflow

![Education channel workflow](/images/handbook/marketing/community-relations/education-oss-workflow.png)

### Step by step

1. All data submitted through forms on our website land on Salesforce
   - as **Lead** object for new users
     - Make sure that we didn't already approve a free license for that *Company*, and that *Billing Address* is entered correctly
     - Convert Lead to **Opportunity**
       - *Opportunity name* - `CompanyName-NumberOfUsers Product Source [w/ Support]` (e.g. ABC University-100 Ultimate EDU or ABC University-100 Ultimate EDU w/ Support)
       - *Account Name* - Attach to Existing if possible, or Create New Account if not
       - Turn off the *Reminder*
   - or as an addition to the existing **Contact** object for existing users
     - Click on 'New Opportunity' and follow the same steps as for leads
       - *Initial Source* - EDU/OSS
       - *Close date* - Today
       - *Stage* - '00-Pre Opportunity'
1. Select the newly created **Opportunity** and create a new **Quote**
   - Select New Billing Account (only for the existing users)
   - *Quote Template* - `Ultimate or Gold Free (EDU and OSS)`
   - *Sold to* and *Bill to Contacts* are the Primary Contact
   - *Start Date* - Today
   - Turn off *Auto Renew* and click `Next`
   - *Add base product* - select Ultimate or Gold for EDU/OSS with or without support and click `Save`
   - *Quantity* - enter the number of users and click `Submit`
1. Generate PDF
1. Send the **Quote** for signing
   - Click *Sertifi eSign*
   - Use an adequate Sertifi eSign email template and click `Next`
   - Select the newly created PDF and verify it is rendered properly with `Preview`
   - Send the Quote for Signature and click `Return`
1. Hover over opportunity link and click `Edit`
   - *Type* - New Business
   - *Close date* - Today
   - *Stage* - 6-Awaiting Signature  

After the quote is signed you need to close the opportunity.
- Click `Submit for Approval`
  - *Type* - New Business
  - *Close Date* - Today
  - *Stage* - 6-Awaiting Signature
  - *Amount* - 0.00 or the annual amount of the support (if included)  
- Once approved, Deal Desk will send the quote to Zuora, and the EULA will be automatically sent. After it is accepted, the license key will be sent (for Ultimate), or instructions for authenticating a group (for Gold). Also, a renewal opportunity will be created.  

Additional steps for OSS program:
- Check if the user followed the rules when submitting a MR:
  - If the `GitLab OSS project URL` is valid
  - If they are using an OSI approved license
  - If they added themselves to the [OSS project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program) README  

## Best practices

### [Customers portal](https://customers.gitlab.com/admin/)
It can be used for:
- Tracking, editing and resending EULAs
- Changing the Primary contacts of the approved subscriptions (only that contact can login and apply subscription)
- Manually upgrading user's GitLab.com groups

### [LicenseApp](https://license.gitlab.com/)
- Can be used for downloading and resending the license keys for the self-hosted subscriptions
- To access LicenseApp, you need to have a dev.gitlab.org account

### Changing EDU/OSS terms
1. Send a legal request to `legal@gitlab.com` and wait for approval
1. Navigate to **Quote**, `generate Word`, modify it (amend new terms at the bottom of the quote) and export as pdf
1. Navigate to **Oportunity**, hover over `Notes & Attachments` and click `Attach File`
   - Choose the updated pdf, click `Attach File` and then click `Done`
1. Once the quote is approved, the EULA will automatically be sent to the user, but it won't reflect the changes that were proposed
   - Override the EULA acceptance by manually entering today's date in customers portal
   - This won't trigger the LicenseApp to send the license key, so ping Ruben to send it manually

### Creating an amendment for the existing subscriptions (adding more seats)

1. Navigate to the original New Business (Closed Won) **Opportunity** and click `New Add On Opportunity`
   - *Opportunity name* - `CompanyName-Add [Quantity] [Product]` (e.g. Oxford University- Add 25 Ultimate)
   - Update: *Initial Source* to EDU/OSS, *Close date* to Today and *Stage* to '00-Pre Opportunity'
1. Select newly created **Opportunity** and create a new **Quote**
   - Update: *Select Billing Account* to `existing billing account` and *Choose Quote Type* to `ammend existing subscription for this billing account` and click `Next`
   - Click `Next` again (you can change signer if needed)
   - Click on *+ Add Products*, select Ultimate or Gold and click `Save`
   - Adjust the *Quantity* to however NEW users they want to add
   - Find where the old product purchase is listed, click downward arrow and click `Remove` and then `Submit`
1. Generate pdf or Word document and proceed as usual

Once the the quote is singed and approved, the LicenseApp will provision a new key for the increased number of users

### Refund process

- Send an email to ar@gitlab.com with a link to the appropriate **Opportunity** and ask them to provide a refund request (they will create a refund opportunity)
- Communicate to the customer that they should see that payment reflect back to their records within 5-7 business days
- Proceed with new quotes/opportunities only when the refund process is done

### Adding Credit Card details

- The customer cannot pay directly by credit card. Rather, they need to add their credit card details on [customers.gitlab.com](https://customers.gitlab.com/).
- To do so, they need to follow these instructions:
  - Log into your account at [GitLab Subscription Manager](https://customers.gitlab.com/customers/sign_in)
  - Click on *Payment Methods* at the top of the page
  - Click on *Add new payment method*
  - Select *Credit Card* as the type
  - Enter the details and submit
  - Please send an e-mail to ar@gitlab.com once the credit card has been added and we will process the payment.

### Other

- [Naming conventions](https://about.gitlab.com/handbook/business-ops/#named-account-ownership)
  - Lead status: *Qualifying* - Advocate reached out to the user and asked for additional information
  - Lead status: *Unqualified* - User is not qualified for that program
- When you need to reassign a SFDC object from *Sales Admin* to *Community Advocate*, just click `Change` next to the object owner field, select `Community Advocate` and click `Save`
- **Case** objects: click `Close Case`, change *Status* to `Closed`, *Case Reason* to `Other` and leave an internal comment with actions you took to resolve it
- Merging duplicated accounts in Salesforce: Make sure the domains and the account names are the same and reach out to Francis
- Payment options: ONLY when the **Quote** is approved and the license key delivered, our billing department will send an invoice where users can choose to pay via credit card, wire transfer or PO
- Handle one-license-per-institution rule on a case-by-case basis. We should first try to work with the institution so that they can manage their master account from a central department. Only if that does not work, we can discuss whether we can issue multiple licenses. 

### Response templates

#### Authenticating Gold groups

```
Information for GitLab.com Gold Upgrades: Groups must follow these instructions to authenticate.

Your subscription has been uploaded and you may follow these instructions to authenticate your groups:

1. Please visit https://customers.gitlab.com/customers/password/new to reset your account password
2. After logging in, please access the "Subscriptions" menu
3. You'll be able to click on "Edit" over a subscription
4. You'll be redirected to GitLab.com for OAuth login
5. At this point, you need to make sure you're logging in using the account you want to license on GitLab.com
6. Please select the Group you want to license then click onto "Update"

Please let me know if you have any questions, always happy to help.

Regards,
YOUR_NAME
```

#### Sertifi eSign

```
Hello,

We’re excited to tell you that your application for the GitLab (EDU or OSS) program has been approved and processed.

We’re sending you a quote that you need to sign after which you’ll have to accept our EULA. Upon acceptance, you’ll get the license key delivered to you via mail (for Ultimate), or further instructions on how to authenticate your groups (for Gold).

Regards,
YOUR_NAME
```

#### Education Applications rejection

```
Hi,

Thanks for applying to GitLab’s EDU program.

Unfortunately, we couldn’t accept your application. Please refer to the eligibility requirements listed at https://about.gitlab.com/education.

A common reason for rejection is students applying on behalf of their University. We’re only able to accept applications submitted by University staff, as part of the process is signing a legal document on behalf of the University.

Another common reason is duplicate applications. We’re only able to accept one application per University. This isn’t to discriminate against smaller independent groups at universities, but rather to prevent the program from growing into unmaintainability.

Regards,
YOUR_NAME
```

## Automation

TBD: this needs to be explained in more detail

Application form -> Marketo -> Salesforce -> Zendesk
